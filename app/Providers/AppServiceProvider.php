<?php

namespace App\Providers;

use App\Contracts\Repositories\UserRepositoryContract;
use App\Contracts\Services\User\UserCreateServiceContract;
use App\Contracts\Services\User\UserDeleteServiceContract;
use App\Contracts\Services\User\UserLessonServiceContract;
use App\Contracts\Services\User\UserManagementServiceContract;
use App\Contracts\Services\User\UserPasswordResetServiceContract;
use App\Contracts\Services\User\UserRatingServiceContract;
use App\Contracts\Services\User\UserUpdateServiceContract;
use App\Repositories\UserRepository;
use App\Services\UserPasswordService;
use App\Services\UserRatingService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->registerServices();
        $this->registerRepositories();

        Inertia::version(fn () => md5_file(public_path('mix-manifest.json')));
    }

    public function boot(): void
    {
        //
    }

    private function registerServices(): void
    {
        $this->app->singleton(UserCreateServiceContract::class, UserService::class);
        $this->app->singleton(UserUpdateServiceContract::class, UserService::class);
        $this->app->singleton(UserDeleteServiceContract::class, UserService::class);
        $this->app->singleton(UserManagementServiceContract::class, UserService::class);
        $this->app->singleton(UserLessonServiceContract::class, UserService::class);

        $this->app->singleton(UserPasswordResetServiceContract::class, UserPasswordService::class);
        $this->app->singleton(UserRatingServiceContract::class, UserRatingService::class);
    }

    private function registerRepositories(): void
    {
        $this->app->singleton(UserRepositoryContract::class, UserRepository::class);
    }
}
