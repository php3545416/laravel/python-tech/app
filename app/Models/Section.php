<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $serial_number
 * @property string $code
 * @property bool $is_active
 */
class Section extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getTitleAttribute(): string
    {
        return "$this->serial_number $this->name";
    }
}
