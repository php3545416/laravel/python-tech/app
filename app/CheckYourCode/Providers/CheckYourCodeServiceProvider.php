<?php

namespace App\CheckYourCode\Providers;

use App\CheckYourCode\CheckYourCodeClient;
use App\CheckYourCode\Contracts\CheckYourCodeClientContract;
use Illuminate\Support\ServiceProvider;

class CheckYourCodeServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(
            CheckYourCodeClientContract::class,
            fn () => $this->app->make(CheckYourCodeClient::class, [
                'baseUrl' => config('services.check_your_code.base_url'),
            ]),
        );
    }

    public function boot(): void
    {
        //
    }
}
