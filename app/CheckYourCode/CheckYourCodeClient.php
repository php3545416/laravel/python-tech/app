<?php

namespace App\CheckYourCode;

use App\CheckYourCode\Contracts\CheckYourCodeClientContract;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

readonly class CheckYourCodeClient implements CheckYourCodeClientContract
{
    public function __construct(
        private string $baseUrl,
    ) {
    }

    /**
     * @throws RequestException
     */
    protected function sendRequest(string $method, string $url, array $headers = [], array $data = []): Response
    {
        /** @var Response $response */

        $response = Http::baseUrl($this->baseUrl)
            ->withHeaders($headers)
            ->asJson()
            ->{$method}($url, $data);

        if ($response->failed()) {
            $response->throw();
        }

        return $response;
    }

    /**
     * @throws RequestException
     */
    public function sendCheckCodeRequest(string $code): array
    {
        return $this->sendRequest(method: 'post', url: '/', data: ['code_area' => $code])->json();
    }
}
