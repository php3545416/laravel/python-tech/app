<?php

namespace App\CheckYourCode\Contracts;

interface CheckYourCodeClientContract
{
    public function sendCheckCodeRequest(string $code): array;
}
