<?php

namespace App\Services;

use App\Contracts\Repositories\UserRepositoryContract;
use App\Contracts\Services\User\UserCreateServiceContract;
use App\Contracts\Services\User\UserDeleteServiceContract;
use App\Contracts\Services\User\UserLessonServiceContract;
use App\Contracts\Services\User\UserManagementServiceContract;
use App\Contracts\Services\User\UserUpdateServiceContract;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Throwable;

readonly class UserService implements UserCreateServiceContract, UserDeleteServiceContract, UserLessonServiceContract, UserManagementServiceContract, UserUpdateServiceContract
{
    public function __construct(
        private UserRepositoryContract $userRepository,
    ) {
    }

    public function create(array $data): User
    {
        $user = $this->userRepository->create($data);

        try {
            event(new Registered($user));
        } catch (Throwable $e) {
            Log::error("Не удалось обработать вызов события регистрации нового пользователя. {$e->getMessage()}", [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage(),
                'error_trace' => $e->getTrace(),
            ]);
        }

        Auth::login($user);

        return $user;
    }

    public function update(int $id, array $data): bool
    {
        return $this->userRepository->update($id, $data);
    }

    public function userEmailVerified(int $id): bool
    {
        return $this->userRepository->userEmailVerified($id);
    }

    public function delete(int $id): ?bool
    {
        return $this->userRepository->delete($id);
    }

    public function addALessonRead(int $userId, int $lessonId): Model
    {
        return $this->userRepository->linkReadLesson($userId, $lessonId);
    }
}
