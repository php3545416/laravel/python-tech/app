<?php

namespace App\Services;

use App\Contracts\Services\User\UserPasswordResetServiceContract;
use App\Contracts\Services\User\UserUpdateServiceContract;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Password;

readonly class UserPasswordService implements UserPasswordResetServiceContract
{
    public function __construct(
        private UserUpdateServiceContract $userUpdateService,
    ) {
    }

    public function sendResetLink(array $credentials): array
    {
        $status = Password::sendResetLink($credentials);

        if ($status == Password::RESET_LINK_SENT) {
            return [true, $status];
        }

        return [false, $status];
    }

    public function reset(array $credentials, array $toUpdate): array
    {
        $status = Password::reset(
            $credentials,
            function ($user) use ($toUpdate) {
                $this->userUpdateService->update($user->id, $toUpdate);

                event(new PasswordReset($user));
            }
        );

        if ($status == Password::PASSWORD_RESET) {
            return [true, $status];
        }

        return [false, $status];
    }
}
