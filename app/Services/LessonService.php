<?php

namespace App\Services;

use App\Models\Lesson;
use App\Repositories\LessonRepository;

readonly class LessonService
{
    public function __construct(
        private LessonRepository $lessonRepository,
    ) {
    }

    public function getLessonById(int $lessonId, int $userId): Lesson
    {
        return $this->lessonRepository->getByIdWithUserById($lessonId, $userId);
    }
}
