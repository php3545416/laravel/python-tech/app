<?php

namespace App\Services;

use App\Contracts\Repositories\UserRepositoryContract;
use App\Contracts\Services\User\UserRatingServiceContract;
use App\Models\User;
use Illuminate\Support\Collection;

readonly class UserRatingService implements UserRatingServiceContract
{
    public function __construct(
        private UserRepositoryContract $userRepository,
    ) {
    }

    public function getUsersRatingsTable(): Collection
    {
        return $this->userRepository
            ->getUsersWithLessonsCount()
            ->filter(fn (User $user) => $user->lessons_count)
            ->sortByDesc('lessons_count');
    }
}
