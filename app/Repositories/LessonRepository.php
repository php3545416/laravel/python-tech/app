<?php

namespace App\Repositories;

use App\Models\Lesson;
use Illuminate\Database\Eloquent\Model;

class LessonRepository
{
    private function getModel(): Lesson
    {
        return app(Lesson::class);
    }

    /**
     * @return Model|Lesson
     */
    public function getById(int $id, array $with = []): Lesson
    {
        return $this->getModel()->newQuery()->with($with)->findOrFail($id);
    }

    /**
     * @param  int  $id
     * @return Model|Lesson
     */
    public function getByIdWithUserById(int $lessonId, int $userId): Lesson
    {
        return $this->getModel()
            ->newQuery()
            ->with(['users' => fn ($query) => $query->where('user_id', $userId)])
            ->findOrFail($lessonId);
    }
}
