<?php

namespace App\Repositories;

use App\Models\Section;
use Illuminate\Support\Collection;

class SectionRepository
{
    private function getModel(): Section
    {
        return app(Section::class);
    }

    public function getList(array $with = []): Collection
    {
        return $this->getModel()->with($with)->get();
    }
}
