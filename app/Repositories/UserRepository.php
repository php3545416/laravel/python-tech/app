<?php

namespace App\Repositories;

use App\Contracts\Repositories\UserRepositoryContract;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class UserRepository implements UserRepositoryContract
{
    private function getModel(): User
    {
        return app(User::class);
    }

    /**
     * @return User|Model
     */
    public function create(array $data): User
    {
        return $this->getModel()->newQuery()->create($data);
    }

    public function update(int $id, array $data): bool
    {
        return $this->getModel()->newQuery()->findOrFail($id)->update($data);
    }

    public function userEmailVerified(int $id): bool
    {
        return (bool) $this->getModel()->newQuery()->findOrFail($id)->email_verified_at;
    }

    public function delete(int $id): ?bool
    {
        return $this->getModel()->newQuery()->firstWhere('id', $id)->delete();
    }

    public function getUsersWithLessonsCount(): Collection
    {
        return $this->getModel()->newQuery()->withCount('lessons')->get();
    }

    /**
     * @return User|Model
     */
    public function linkReadLesson(int $userId, int $lessonId): User
    {
        $user = $this->getModel()->newQuery()->findOrFail($userId);

        $user->lessons()->attach($lessonId);

        return $user;
    }
}
