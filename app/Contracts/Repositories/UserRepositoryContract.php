<?php

namespace App\Contracts\Repositories;

use App\Models\User;
use Illuminate\Support\Collection;

interface UserRepositoryContract
{
    public function create(array $data): User;

    public function update(int $id, array $data): bool;

    public function userEmailVerified(int $id): bool;

    public function delete(int $id): ?bool;

    public function getUsersWithLessonsCount(): Collection;

    public function linkReadLesson(int $userId, int $lessonId): User;
}
