<?php

namespace App\Contracts\Services\User;

interface UserPasswordResetServiceContract
{
    public function sendResetLink(array $credentials): array;

    public function reset(array $credentials, array $toUpdate): array;
}
