<?php

namespace App\Contracts\Services\User;

interface UserDeleteServiceContract
{
    public function delete(int $id): ?bool;
}
