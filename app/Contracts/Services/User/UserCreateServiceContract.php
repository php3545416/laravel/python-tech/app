<?php

namespace App\Contracts\Services\User;

use App\Models\User;

interface UserCreateServiceContract
{
    public function create(array $data): User;
}
