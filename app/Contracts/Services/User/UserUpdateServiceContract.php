<?php

namespace App\Contracts\Services\User;


interface UserUpdateServiceContract
{
    public function update(int $id, array $data): bool;
}
