<?php

namespace App\Contracts\Services\User;

interface UserManagementServiceContract
{
    public function userEmailVerified(int $id): bool;
}
