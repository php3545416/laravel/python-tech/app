<?php

namespace App\Contracts\Services\User;

use Illuminate\Database\Eloquent\Model;

interface UserLessonServiceContract
{

    public function addALessonRead(int $userId, int $lessonId): Model;
}
