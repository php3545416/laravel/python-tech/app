<?php

namespace App\Contracts\Services\User;

use Illuminate\Support\Collection;

interface UserRatingServiceContract
{
    public function getUsersRatingsTable(): Collection;
}
