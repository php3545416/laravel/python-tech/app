<?php

namespace App\Http\Requests\Registered;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

/**
 * @property string $password
 * @property string $name
 * @property string $email
 */
class RegisteredUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|lowercase|email|max:255|unique:'.User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults(fn () => Rules\Password::min(4))],
        ];
    }

    protected function passedValidation(): void
    {
        $this->replace([
            'password' => Hash::make($this->password),
        ]);
    }
}
