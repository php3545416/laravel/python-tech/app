<?php

namespace App\Http\Requests\CheckYourCode;

use Illuminate\Foundation\Http\FormRequest;

class CheckYourCodeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'codeArea' => 'required|string',
        ];
    }
}
