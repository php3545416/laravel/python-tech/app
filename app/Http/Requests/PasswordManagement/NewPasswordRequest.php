<?php

namespace App\Http\Requests\PasswordManagement;

use Illuminate\Support\Str;

/**
 * @property string $email
 * @property string $token
 */
class NewPasswordRequest extends BasePasswordRequest
{
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'token' => 'required',
            'email' => 'required|email',
            'password_confirmation' => 'required|same:password',
            'remember_token' => 'required|string|max:60|min:60',
        ]);
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'remember_token' => Str::random(60),
        ]);
    }
}
