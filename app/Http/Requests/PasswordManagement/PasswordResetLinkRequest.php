<?php

namespace App\Http\Requests\PasswordManagement;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $email
 */
class PasswordResetLinkRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|email',
        ];
    }
}
