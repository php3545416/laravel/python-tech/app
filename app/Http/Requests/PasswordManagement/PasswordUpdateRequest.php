<?php

namespace App\Http\Requests\PasswordManagement;

class PasswordUpdateRequest extends BasePasswordRequest
{
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'current_password' => ['required', 'current_password'],
        ]);
    }
}
