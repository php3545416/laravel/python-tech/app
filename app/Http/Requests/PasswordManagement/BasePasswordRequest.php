<?php

namespace App\Http\Requests\PasswordManagement;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

/**
 * @property string $password
 */
class BasePasswordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'password' => ['required', 'confirmed', Rules\Password::defaults(fn () => Rules\Password::min(4))],
        ];
    }

    protected function passedValidation(): void
    {
        $this->replace([
            'password' => Hash::make($this->password),
        ]);
    }
}
