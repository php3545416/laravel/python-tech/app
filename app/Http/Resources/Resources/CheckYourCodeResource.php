<?php

namespace App\Http\Resources\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CheckYourCodeResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'code' => $this->resource['code'],
            'output' => $this->resource['output'],
        ];
    }
}
