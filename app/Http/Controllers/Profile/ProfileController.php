<?php

namespace App\Http\Controllers\Profile;

use App\Contracts\Services\User\UserDeleteServiceContract;
use App\Contracts\Services\User\UserRatingServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\ProfileUpdateRequest;
use App\Http\Requests\Profile\UserDestroyRequest;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class ProfileController extends Controller
{
    public function __construct(
        private readonly UserDeleteServiceContract $userDeleteService,
        private readonly UserRatingServiceContract $userRatingService,
    ) {
    }

    public function index(Request $request): Response
    {
        return Inertia::render('Profile/Edit', [
            'mustVerifyEmail' => $request->user() instanceof MustVerifyEmail,
            'status' => session('status'),
            'topUsers' => $this->userRatingService->getUsersRatingsTable(),
        ]);
    }

    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->validated());

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        $request->user()->save();

        return Redirect::route('profile.index');
    }

    public function destroy(UserDestroyRequest $request): RedirectResponse
    {
        $user = $request->user();

        Auth::logout();

        $deleted = $this->userDeleteService->delete($user->id);

        if (! is_null($deleted)) {
            $request->session()->invalidate();
            $request->session()->regenerateToken();
        }

        return Redirect::to('/');
    }
}
