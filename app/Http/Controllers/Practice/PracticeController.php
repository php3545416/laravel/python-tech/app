<?php

namespace App\Http\Controllers\Practice;

use App\CheckYourCode\Contracts\CheckYourCodeClientContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckYourCode\CheckYourCodeRequest;
use App\Http\Resources\Resources\CheckYourCodeResource;
use Inertia\Inertia;
use Inertia\Response;

class PracticeController extends Controller
{
    public function __construct(
        private readonly CheckYourCodeClientContract $checkYourCodeClient,
    ) {
    }

    public function index(): Response
    {
        return Inertia::render('Practice/Practice');
    }

    public function store(CheckYourCodeRequest $request): Response
    {
        $response = $this->checkYourCodeClient->sendCheckCodeRequest($request->validated('codeArea'));

        return Inertia::render(
            'Practice/Practice',
            CheckYourCodeResource::make($response)->resolve(),
        );
    }
}
