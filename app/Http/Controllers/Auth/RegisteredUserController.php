<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\Services\User\UserCreateServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Registered\RegisteredUserRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class RegisteredUserController extends Controller
{
    public function __construct(
        readonly private UserCreateServiceContract $userService,
    ) {
    }

    public function create(): Response
    {
        return Inertia::render('Auth/Register');
    }

    public function store(RegisteredUserRequest $request): RedirectResponse
    {
        $this->userService->create($request->validated());

        return redirect(RouteServiceProvider::HOME);
    }
}
