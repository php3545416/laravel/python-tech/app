<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\Services\User\UserPasswordResetServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordManagement\PasswordResetLinkRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Inertia\Response;

class PasswordResetLinkController extends Controller
{
    public function __construct(
        private readonly UserPasswordResetServiceContract $userPasswordResetService,
    ) {
    }

    public function create(): Response
    {
        return Inertia::render('Auth/ForgotPassword', [
            'status' => session('status'),
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function store(PasswordResetLinkRequest $request): RedirectResponse
    {
        [$success, $status] = $this->userPasswordResetService->sendResetLink(
            credentials: $request->only('email'),
        );

        if (! $success) {
            throw ValidationException::withMessages([
                'email' => [trans($status)],
            ]);
        }

        return back()->with('status', __($status));
    }
}
