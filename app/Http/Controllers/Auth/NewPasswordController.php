<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\Services\User\UserPasswordResetServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordManagement\NewPasswordRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Inertia\Response;

class NewPasswordController extends Controller
{
    public function __construct(
        private readonly UserPasswordResetServiceContract $userPasswordResetService,
    ) {
    }

    public function create(Request $request): Response
    {
        return Inertia::render('Auth/ResetPassword', [
            'email' => $request->email,
            'token' => $request->route('token'),
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function store(NewPasswordRequest $request): RedirectResponse
    {
        [$success, $status] = $this->userPasswordResetService->reset(
            $request->safe(['email', 'password', 'password_confirmation', 'token']),
            $request->safe(['password', 'remember_token']),
        );

        if (! $success) {
            throw ValidationException::withMessages([
                'email' => [trans($status)],
            ]);
        }

        return redirect()->route('login')->with('status', __($status));
    }
}
