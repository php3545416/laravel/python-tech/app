<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\Services\User\UserUpdateServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordManagement\PasswordUpdateRequest;
use Illuminate\Http\RedirectResponse;

class PasswordController extends Controller
{
    public function __construct(
        private readonly UserUpdateServiceContract $userUpdateService,
    ) {
    }

    public function update(PasswordUpdateRequest $request): RedirectResponse
    {
        $this->userUpdateService->update($request->user()->id, $request->validated());

        return back();
    }
}
