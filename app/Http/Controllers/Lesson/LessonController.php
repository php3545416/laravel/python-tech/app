<?php

namespace App\Http\Controllers\Lesson;

use App\Contracts\Services\User\UserLessonServiceContract;
use App\Http\Controllers\Controller;
use App\Services\LessonService;
use Inertia\Inertia;
use Inertia\Response;

class LessonController extends Controller
{
    public function __construct(
        private readonly UserLessonServiceContract $userLessonService,
        private readonly LessonService $lessonService,
    ) {
    }

    public function index(): Response
    {
        return Inertia::render('Lessons/Index');
    }

    public function show(string $id): Response
    {
        $lesson = $this->lessonService->getLessonById($id, auth()->user()->id);

        return Inertia::render('Lessons/Lesson', [
            'lesson' => $lesson,
            'isRead' => $lesson->users->isNotEmpty(),
        ]);
    }

    public function complete(string $id): Response
    {
        $this->userLessonService->addALessonRead(auth()->user()->id, $id);

        $lesson = $this->lessonService->getLessonById($id, auth()->user()->id);

        return Inertia::render('Lessons/Lesson', [
            'lesson' => $lesson,
            'isRead' => true,
        ]);
    }
}
