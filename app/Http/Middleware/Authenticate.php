<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Authenticate extends Middleware
{
    protected function redirectTo(Request $request): ?string
    {
        $reasons = $this->reasons();

        if ($request->expectsJson()) {
            return null;
        }

        Session::flash('requireAuth', $reasons[$request->route()->getName()] ?? 'Необходимо авторизоваться!');

        return url()->previous();
    }

    private function reasons(): array
    {
        return [
            'profile.index' => [
                ...$this->baseReasonTitle(),
                'description' => 'Для перехода в профиль необходимо авторизоваться',
            ],
            'practice.index' => [
                ...$this->baseReasonTitle(),
                'description' => 'Для перехода на страницу практического задания необходимо авторизоваться',
            ],
        ];
    }

    private function baseReasonTitle(): array
    {
        return ['title' => 'Необходимо авторизоваться'];
    }
}
