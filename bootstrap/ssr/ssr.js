import { useSSRContext, mergeProps, unref, withCtx, createVNode, useModel, ref, onMounted, resolveComponent, createTextVNode, computed, renderSlot, defineComponent, watch, toDisplayString, onUnmounted, withKeys, nextTick, createSSRApp, h } from "vue";
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderSlot, ssrInterpolate, ssrGetDynamicModelProps, ssrLooseContain, ssrRenderAttr, ssrRenderStyle, ssrRenderList, ssrRenderTeleport, ssrRenderClass } from "vue/server-renderer";
import { Link, Head, useForm, usePage, createInertiaApp } from "@inertiajs/vue3";
import { IonIcon } from "@ionic/vue";
import { addIcons } from "ionicons";
import { arrowForwardOutline } from "ionicons/icons/index.mjs";
import { BModal } from "bootstrap-vue-next";
import Codemirror from "codemirror-editor-vue3";
import "codemirror/mode/python/python.js";
import "codemirror/addon/display/placeholder.js";
import { toNumber } from "lodash";
import { renderToString } from "@vue/server-renderer";
import createServer from "@inertiajs/vue3/server";
const _imports_0$2 = "/build/assets/main-logo-BkmVnOtL.png";
const _export_sfc = (sfc, props) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props) {
    target[key] = val;
  }
  return target;
};
const _sfc_main$t = {};
function _sfc_ssrRender$i(_ctx, _push, _parent, _attrs) {
  _push(`<img${ssrRenderAttrs(mergeProps({
    src: _imports_0$2,
    alt: ""
  }, _attrs))}>`);
}
const _sfc_setup$t = _sfc_main$t.setup;
_sfc_main$t.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/ApplicationLogo.vue");
  return _sfc_setup$t ? _sfc_setup$t(props, ctx) : void 0;
};
const ApplicationLogo = /* @__PURE__ */ _export_sfc(_sfc_main$t, [["ssrRender", _sfc_ssrRender$i]]);
const _sfc_main$s = {
  __name: "GuestLayout",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100" }, _attrs))}><div>`);
      _push(ssrRenderComponent(unref(Link), {
        href: "/",
        "preserve-state": ""
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(ApplicationLogo, null, null, _parent2, _scopeId));
          } else {
            return [
              createVNode(ApplicationLogo)
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div><div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">`);
      ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
      _push(`</div></div>`);
    };
  }
};
const _sfc_setup$s = _sfc_main$s.setup;
_sfc_main$s.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Layouts/GuestLayout.vue");
  return _sfc_setup$s ? _sfc_setup$s(props, ctx) : void 0;
};
const _sfc_main$r = {
  __name: "InputError",
  __ssrInlineRender: true,
  props: {
    message: {
      type: String
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({
        style: __props.message ? null : { display: "none" }
      }, _attrs))}><p class="text-sm text-red-600">${ssrInterpolate(__props.message)}</p></div>`);
    };
  }
};
const _sfc_setup$r = _sfc_main$r.setup;
_sfc_main$r.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/InputError.vue");
  return _sfc_setup$r ? _sfc_setup$r(props, ctx) : void 0;
};
const _sfc_main$q = {
  __name: "InputLabel",
  __ssrInlineRender: true,
  props: {
    value: {
      type: String
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<label${ssrRenderAttrs(mergeProps({ class: "block font-medium text-sm text-gray-700" }, _attrs))}>`);
      if (__props.value) {
        _push(`<span>${ssrInterpolate(__props.value)}</span>`);
      } else {
        _push(`<span>`);
        ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
        _push(`</span>`);
      }
      _push(`</label>`);
    };
  }
};
const _sfc_setup$q = _sfc_main$q.setup;
_sfc_main$q.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/InputLabel.vue");
  return _sfc_setup$q ? _sfc_setup$q(props, ctx) : void 0;
};
const _sfc_main$p = {};
function _sfc_ssrRender$h(_ctx, _push, _parent, _attrs) {
  _push(`<button${ssrRenderAttrs(mergeProps({ class: "inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150" }, _attrs))}>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</button>`);
}
const _sfc_setup$p = _sfc_main$p.setup;
_sfc_main$p.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/PrimaryButton.vue");
  return _sfc_setup$p ? _sfc_setup$p(props, ctx) : void 0;
};
const PrimaryButton = /* @__PURE__ */ _export_sfc(_sfc_main$p, [["ssrRender", _sfc_ssrRender$h]]);
const _sfc_main$o = {
  __name: "TextInput",
  __ssrInlineRender: true,
  props: {
    "modelValue": {
      type: String,
      required: true
    },
    "modelModifiers": {}
  },
  emits: ["update:modelValue"],
  setup(__props, { expose: __expose }) {
    const model = useModel(__props, "modelValue");
    const input = ref(null);
    onMounted(() => {
      if (input.value.hasAttribute("autofocus")) {
        input.value.focus();
      }
    });
    __expose({ focus: () => input.value.focus() });
    return (_ctx, _push, _parent, _attrs) => {
      let _temp0;
      _push(`<input${ssrRenderAttrs((_temp0 = mergeProps({
        class: "border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm",
        ref_key: "input",
        ref: input
      }, _attrs), mergeProps(_temp0, ssrGetDynamicModelProps(_temp0, model.value))))}>`);
    };
  }
};
const _sfc_setup$o = _sfc_main$o.setup;
_sfc_main$o.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/TextInput.vue");
  return _sfc_setup$o ? _sfc_setup$o(props, ctx) : void 0;
};
const _sfc_main$n = {
  components: {
    Head,
    TextInput: _sfc_main$o,
    PrimaryButton,
    InputLabel: _sfc_main$q,
    InputError: _sfc_main$r,
    GuestLayout: _sfc_main$s
  },
  layout: _sfc_main$s,
  props: {},
  setup(props) {
    const form = useForm({
      password: ""
    });
    const submit = () => {
      form.post("/confirm-password", {
        onFinish: () => form.reset()
      });
    };
    return {
      form,
      submit
    };
  }
};
function _sfc_ssrRender$g(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputLabel = resolveComponent("InputLabel");
  const _component_TextInput = resolveComponent("TextInput");
  const _component_InputError = resolveComponent("InputError");
  const _component_PrimaryButton = resolveComponent("PrimaryButton");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Проверка пароля" }, null, _parent));
  _push(`<div class="mb-4 text-sm text-gray-600"> Это обычная проверка безопасности. Пожалуйста, подтвердите свой пароль, прежде чем продолжить. </div><form><div>`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "password",
    value: "Пароль"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "password",
    type: "password",
    class: "mt-1 block w-full",
    modelValue: $setup.form.password,
    "onUpdate:modelValue": ($event) => $setup.form.password = $event,
    required: "",
    placeholder: "*********",
    autofocus: ""
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.password
  }, null, _parent));
  _push(`</div><div class="flex justify-end mt-4">`);
  _push(ssrRenderComponent(_component_PrimaryButton, {
    class: ["ms-4", { "opacity-25": $setup.form.processing }],
    disabled: $setup.form.processing
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Подтвердить `);
      } else {
        return [
          createTextVNode(" Подтвердить ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></form><!--]-->`);
}
const _sfc_setup$n = _sfc_main$n.setup;
_sfc_main$n.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/ConfirmPassword.vue");
  return _sfc_setup$n ? _sfc_setup$n(props, ctx) : void 0;
};
const ConfirmPassword = /* @__PURE__ */ _export_sfc(_sfc_main$n, [["ssrRender", _sfc_ssrRender$g]]);
const __vite_glob_0_0 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: ConfirmPassword
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$m = {
  components: {
    TextInput: _sfc_main$o,
    PrimaryButton,
    InputLabel: _sfc_main$q,
    InputError: _sfc_main$r,
    GuestLayout: _sfc_main$s,
    Head
  },
  layout: _sfc_main$s,
  props: {
    status: {
      type: String
    }
  },
  setup(props) {
    const form = useForm({
      email: ""
    });
    const submit = () => {
      form.post("/forgot-password");
    };
    return {
      form,
      submit
    };
  }
};
function _sfc_ssrRender$f(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputLabel = resolveComponent("InputLabel");
  const _component_TextInput = resolveComponent("TextInput");
  const _component_InputError = resolveComponent("InputError");
  const _component_PrimaryButton = resolveComponent("PrimaryButton");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Восстановление пароля" }, null, _parent));
  _push(`<div class="mb-4 text-sm text-gray-600"> Забыли пароль? Не проблема! Просто укажите почту, на которую хотите получить ссылку для сброса пароля </div>`);
  if ($props.status) {
    _push(`<div class="mb-4 font-medium text-sm text-green-600">${ssrInterpolate($props.status)}</div>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<form><div>`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "email",
    value: "Почта"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "email",
    type: "email",
    class: "mt-1 block w-full",
    modelValue: $setup.form.email,
    "onUpdate:modelValue": ($event) => $setup.form.email = $event,
    required: "",
    autofocus: "",
    autocomplete: "username"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.email
  }, null, _parent));
  _push(`</div><div class="flex items-center justify-end mt-4">`);
  _push(ssrRenderComponent(_component_PrimaryButton, {
    class: { "opacity-25": $setup.form.processing },
    disabled: $setup.form.processing
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Отправить `);
      } else {
        return [
          createTextVNode(" Отправить ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></form><!--]-->`);
}
const _sfc_setup$m = _sfc_main$m.setup;
_sfc_main$m.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/ForgotPassword.vue");
  return _sfc_setup$m ? _sfc_setup$m(props, ctx) : void 0;
};
const ForgotPassword = /* @__PURE__ */ _export_sfc(_sfc_main$m, [["ssrRender", _sfc_ssrRender$f]]);
const __vite_glob_0_1 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: ForgotPassword
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$l = {
  __name: "Checkbox",
  __ssrInlineRender: true,
  props: {
    checked: {
      type: [Array, Boolean],
      required: true
    },
    value: {
      default: null
    }
  },
  emits: ["update:checked"],
  setup(__props, { emit: __emit }) {
    const emit = __emit;
    const props = __props;
    const proxyChecked = computed({
      get() {
        return props.checked;
      },
      set(val) {
        emit("update:checked", val);
      }
    });
    return (_ctx, _push, _parent, _attrs) => {
      let _temp0;
      _push(`<input${ssrRenderAttrs((_temp0 = mergeProps({
        type: "checkbox",
        value: __props.value,
        checked: Array.isArray(proxyChecked.value) ? ssrLooseContain(proxyChecked.value, __props.value) : proxyChecked.value,
        class: "rounded border-gray-300 text-indigo-600 shadow focus:ring-indigo-500"
      }, _attrs), mergeProps(_temp0, ssrGetDynamicModelProps(_temp0, proxyChecked.value))))}>`);
    };
  }
};
const _sfc_setup$l = _sfc_main$l.setup;
_sfc_main$l.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/Checkbox.vue");
  return _sfc_setup$l ? _sfc_setup$l(props, ctx) : void 0;
};
const _sfc_main$k = {
  components: {
    Checkbox: _sfc_main$l,
    InputError: _sfc_main$r,
    InputLabel: _sfc_main$q,
    PrimaryButton,
    TextInput: _sfc_main$o,
    Head,
    Link
  },
  layout: _sfc_main$s,
  props: {
    canResetPassword: {
      type: Boolean
    },
    status: {
      type: String
    }
  },
  setup(props) {
    const form = useForm({
      email: "",
      password: "",
      remember: false
    });
    const submit = () => {
      form.post("/login", {
        onFinish: () => form.reset("password")
      });
    };
    return {
      form,
      submit
    };
  }
};
function _sfc_ssrRender$e(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputLabel = resolveComponent("InputLabel");
  const _component_TextInput = resolveComponent("TextInput");
  const _component_InputError = resolveComponent("InputError");
  const _component_Checkbox = resolveComponent("Checkbox");
  const _component_Link = resolveComponent("Link");
  const _component_PrimaryButton = resolveComponent("PrimaryButton");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Вход" }, null, _parent));
  if ($props.status) {
    _push(`<div class="mb-4 font-medium text-sm text-green-600">${ssrInterpolate($props.status)}</div>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<form><div>`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "email",
    value: "Почта"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "email",
    type: "email",
    class: "mt-1 block w-full",
    modelValue: $setup.form.email,
    "onUpdate:modelValue": ($event) => $setup.form.email = $event,
    required: "",
    autofocus: "",
    placeholder: "example@gmail.com"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.email
  }, null, _parent));
  _push(`</div><div class="mt-4">`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "password",
    value: "Пароль"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "password",
    type: "password",
    class: "mt-1 block w-full",
    modelValue: $setup.form.password,
    "onUpdate:modelValue": ($event) => $setup.form.password = $event,
    required: "",
    placeholder: "*******"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.password
  }, null, _parent));
  _push(`</div><div class="block mt-4"><label class="flex items-center">`);
  _push(ssrRenderComponent(_component_Checkbox, {
    name: "remember",
    checked: $setup.form.remember,
    "onUpdate:checked": ($event) => $setup.form.remember = $event
  }, null, _parent));
  _push(`<span class="ms-2 text-sm text-gray-600">Запомнить меня</span></label></div><div class="flex items-center justify-end mt-4">`);
  if ($props.canResetPassword) {
    _push(ssrRenderComponent(_component_Link, {
      href: "/forgot-password",
      class: "underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
    }, {
      default: withCtx((_, _push2, _parent2, _scopeId) => {
        if (_push2) {
          _push2(` Забыли пароль? `);
        } else {
          return [
            createTextVNode(" Забыли пароль? ")
          ];
        }
      }),
      _: 1
    }, _parent));
  } else {
    _push(`<!---->`);
  }
  _push(ssrRenderComponent(_component_PrimaryButton, {
    class: ["ms-4", { "opacity-25": $setup.form.processing }],
    disabled: $setup.form.processing
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Войти `);
      } else {
        return [
          createTextVNode(" Войти ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></form><!--]-->`);
}
const _sfc_setup$k = _sfc_main$k.setup;
_sfc_main$k.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/Login.vue");
  return _sfc_setup$k ? _sfc_setup$k(props, ctx) : void 0;
};
const Login = /* @__PURE__ */ _export_sfc(_sfc_main$k, [["ssrRender", _sfc_ssrRender$e]]);
const __vite_glob_0_2 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Login
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$j = {
  components: { InputError: _sfc_main$r, InputLabel: _sfc_main$q, PrimaryButton, TextInput: _sfc_main$o, Head, Link },
  layout: _sfc_main$s,
  props: {},
  setup(props) {
    const form = useForm({
      name: "",
      email: "",
      password: "",
      password_confirmation: ""
    });
    const submit = () => {
      form.post("", {
        onFinish: () => form.reset("password", "password_confirmation")
      });
    };
    return {
      form,
      submit
    };
  }
};
function _sfc_ssrRender$d(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputLabel = resolveComponent("InputLabel");
  const _component_TextInput = resolveComponent("TextInput");
  const _component_InputError = resolveComponent("InputError");
  const _component_Link = resolveComponent("Link");
  const _component_PrimaryButton = resolveComponent("PrimaryButton");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Регистрация" }, null, _parent));
  _push(`<form><div>`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "name",
    value: "ФИО"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "name",
    type: "text",
    class: "mt-1 block w-full",
    modelValue: $setup.form.name,
    "onUpdate:modelValue": ($event) => $setup.form.name = $event,
    required: "",
    autofocus: "",
    placeholder: "Иванов Иван Иванович"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.name
  }, null, _parent));
  _push(`</div><div class="mt-4">`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "email",
    value: "Почта"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "email",
    type: "email",
    class: "mt-1 block w-full",
    modelValue: $setup.form.email,
    "onUpdate:modelValue": ($event) => $setup.form.email = $event,
    required: "",
    placeholder: "example@gmail.com"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.email
  }, null, _parent));
  _push(`</div><div class="mt-4">`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "password",
    value: "Пароль"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "password",
    type: "password",
    class: "mt-1 block w-full",
    modelValue: $setup.form.password,
    "onUpdate:modelValue": ($event) => $setup.form.password = $event,
    required: "",
    placeholder: "*********"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.password
  }, null, _parent));
  _push(`</div><div class="mt-4">`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "password_confirmation",
    value: "Подтвердите пароль"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "password_confirmation",
    type: "password",
    class: "mt-1 block w-full",
    modelValue: $setup.form.password_confirmation,
    "onUpdate:modelValue": ($event) => $setup.form.password_confirmation = $event,
    required: "",
    placeholder: "*********"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.password_confirmation
  }, null, _parent));
  _push(`</div><div class="flex items-center justify-end mt-4">`);
  _push(ssrRenderComponent(_component_Link, {
    href: "/login",
    class: "underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Уже зарегистрированы? `);
      } else {
        return [
          createTextVNode(" Уже зарегистрированы? ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(ssrRenderComponent(_component_PrimaryButton, {
    class: ["ms-4", { "opacity-25": $setup.form.processing }],
    disabled: $setup.form.processing
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Регистрация `);
      } else {
        return [
          createTextVNode(" Регистрация ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></form><!--]-->`);
}
const _sfc_setup$j = _sfc_main$j.setup;
_sfc_main$j.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/Register.vue");
  return _sfc_setup$j ? _sfc_setup$j(props, ctx) : void 0;
};
const Register$1 = /* @__PURE__ */ _export_sfc(_sfc_main$j, [["ssrRender", _sfc_ssrRender$d]]);
const __vite_glob_0_3 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Register$1
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$i = {
  components: {
    Head,
    TextInput: _sfc_main$o,
    PrimaryButton,
    InputLabel: _sfc_main$q,
    InputError: _sfc_main$r,
    GuestLayout: _sfc_main$s
  },
  layout: _sfc_main$s,
  props: {
    email: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  },
  setup(props) {
    const form = useForm({
      token: props.token,
      email: props.email,
      password: "",
      password_confirmation: ""
    });
    const submit = () => {
      form.post("/reset-password", {
        onFinish: () => form.reset("password", "password_confirmation")
      });
    };
    return {
      form,
      submit
    };
  }
};
function _sfc_ssrRender$c(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputLabel = resolveComponent("InputLabel");
  const _component_TextInput = resolveComponent("TextInput");
  const _component_InputError = resolveComponent("InputError");
  const _component_PrimaryButton = resolveComponent("PrimaryButton");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Сброс пароля" }, null, _parent));
  _push(`<form><div>`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "email",
    value: "Почта"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "email",
    type: "email",
    class: "mt-1 block w-full",
    modelValue: $setup.form.email,
    "onUpdate:modelValue": ($event) => $setup.form.email = $event,
    required: "",
    autofocus: "",
    placeholder: "example@gmail.com"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.email
  }, null, _parent));
  _push(`</div><div class="mt-4">`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "password",
    value: "Новый пароль"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "password",
    type: "password",
    class: "mt-1 block w-full",
    modelValue: $setup.form.password,
    "onUpdate:modelValue": ($event) => $setup.form.password = $event,
    required: "",
    placeholder: "*********"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.password
  }, null, _parent));
  _push(`</div><div class="mt-4">`);
  _push(ssrRenderComponent(_component_InputLabel, {
    for: "password_confirmation",
    value: "Подтвердите пароль"
  }, null, _parent));
  _push(ssrRenderComponent(_component_TextInput, {
    id: "password_confirmation",
    type: "password",
    class: "mt-1 block w-full",
    modelValue: $setup.form.password_confirmation,
    "onUpdate:modelValue": ($event) => $setup.form.password_confirmation = $event,
    required: "",
    placeholder: "*********"
  }, null, _parent));
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: $setup.form.errors.password_confirmation
  }, null, _parent));
  _push(`</div><div class="flex items-center justify-end mt-4">`);
  _push(ssrRenderComponent(_component_PrimaryButton, {
    class: { "opacity-25": $setup.form.processing },
    disabled: $setup.form.processing
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Сбросить пароль `);
      } else {
        return [
          createTextVNode(" Сбросить пароль ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></form><!--]-->`);
}
const _sfc_setup$i = _sfc_main$i.setup;
_sfc_main$i.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/ResetPassword.vue");
  return _sfc_setup$i ? _sfc_setup$i(props, ctx) : void 0;
};
const ResetPassword = /* @__PURE__ */ _export_sfc(_sfc_main$i, [["ssrRender", _sfc_ssrRender$c]]);
const __vite_glob_0_4 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: ResetPassword
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$h = {
  components: {
    Head,
    Link,
    PrimaryButton
  },
  layout: _sfc_main$s,
  props: {
    status: {
      type: String
    }
  },
  setup(props) {
    const form = useForm({});
    const submit = () => {
      form.post("/email/verification-notification");
    };
    const verificationLinkSent = computed(() => props.status === "verification-link-sent");
    return {
      form,
      submit,
      verificationLinkSent
    };
  }
};
function _sfc_ssrRender$b(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_PrimaryButton = resolveComponent("PrimaryButton");
  const _component_Link = resolveComponent("Link");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Подтверждение почты" }, null, _parent));
  _push(`<div class="mb-4 text-sm text-gray-600"> Спасибо, что Вы с нами! Не могли бы Вы подтвердить свою почту? Выслали Вам письмо с подтверждением на почту. </div>`);
  if ($setup.verificationLinkSent) {
    _push(`<div class="mb-4 font-medium text-sm text-green-600"> Новая ссылка для подтверждения была отправлена на Вашу почту, указанную при регистрации. </div>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<form><div class="mt-4 flex items-center justify-between">`);
  _push(ssrRenderComponent(_component_PrimaryButton, {
    class: { "opacity-25": $setup.form.processing },
    disabled: $setup.form.processing
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Отправить подтверждение еще раз `);
      } else {
        return [
          createTextVNode(" Отправить подтверждение еще раз ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(ssrRenderComponent(_component_Link, {
    href: "/logout",
    method: "post",
    class: "underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Выйти `);
      } else {
        return [
          createTextVNode(" Выйти ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></form><!--]-->`);
}
const _sfc_setup$h = _sfc_main$h.setup;
_sfc_main$h.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/VerifyEmail.vue");
  return _sfc_setup$h ? _sfc_setup$h(props, ctx) : void 0;
};
const VerifyEmail = /* @__PURE__ */ _export_sfc(_sfc_main$h, [["ssrRender", _sfc_ssrRender$b]]);
const __vite_glob_0_5 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: VerifyEmail
}, Symbol.toStringTag, { value: "Module" }));
const _imports_0$1 = "/build/assets/programming-banner-CtC5gE-m.webp";
const _imports_1$1 = "/build/assets/shape-13-BLRf7Q_k.png";
const _imports_2$1 = "/build/assets/python-icon-CQD1kmAT.png";
const _imports_3 = "/build/assets/code-icon-b1EItz_H.png";
const _imports_4 = "/build/assets/who-1-DhjwrAAE.jpg";
const _imports_5 = "/build/assets/who-2-BNali-u_.jpg";
const _imports_6 = "/build/assets/who-3-BI1zMoAJ.webp";
const _imports_7 = "/build/assets/features1-Odil5TMM.png";
const _imports_8 = "/build/assets/icon3-vnEvCK6c.svg";
const _imports_9 = "/build/assets/features2-BFCPw0eE.png";
const _imports_10 = "/build/assets/icon7-BMUKzmcR.svg";
const _imports_11 = "/build/assets/features3-DfnQ_xjU.png";
const _imports_12 = "/build/assets/icon8-BdF5Eg7Y.svg";
const _imports_13 = "/build/assets/features4-B3cYr6mt.png";
const _imports_14 = "/build/assets/icon4-CFrFsGuH.svg";
const _imports_15 = "/build/assets/overview-CoECZhma.png";
const _imports_16 = "/build/assets/featrues-img-01-Mryk1uvn.webp";
const _imports_17 = "/build/assets/featrues-img-02-D-SeXr8O.webp";
const _imports_18 = "/build/assets/featrues-img-03-BV9T-Q7g.webp";
const _sfc_main$g = {
  components: { IonIcon, Head },
  props: {},
  setup(props) {
    return {};
  }
};
function _sfc_ssrRender$a(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Главная страница" }, null, _parent));
  _push(`<section id="slide"><div class="elementor-background-overlay"></div><div class="main-banner-area parallax"><div class="container"><div class="row"><div class="col-lg-7 col-md-12 col-sm-12 col-xs-12"><div class="main-banner-content block"><span class="metka" data-aos="fade-up">Программирование с нуля</span><h1 data-aos="fade-left">Курс «Python-разработчик»</h1><div class="desc_course" data-aos="fade-up"><p>Курс &quot;Программирование на Python&quot; предназначен для начинающих, желающих овладеть третьим по популярности языком программирования в мире с нуля. Освойте этот язык и параллельно погрузитесь в процесс игры. Данный курс рассчитан на выполнение теоретических и практических заданий.</p><div><div class="h1_buttons"><a href="" class="zapis" data-aos="flip-left">Записаться `);
  _push(ssrRenderComponent(_component_ion_icon, { name: "arrow-forward-outline" }, null, _parent));
  _push(`</a><a href="#programma" class="program" data-aos="flip-left">Посмотреть программу</a></div></div></div></div></div><div class="col-lg-5 col-md-12 col-sm-12 col-xs-12"><div class="main-banner-image"><img class="programming" data-aos="fade-up" data-aos-duration="800"${ssrRenderAttr("src", _imports_0$1)} alt=""><img class="shape"${ssrRenderAttr("src", _imports_1$1)} alt=""><div class=""><div class="parallax__layer"><img class="python-icon" data-aos="fade-down" data-aos-duration="800"${ssrRenderAttr("src", _imports_2$1)} alt=""></div><div class="parallax__layer"><img class="code-icon" data-aos="fade-down" data-aos-duration="800"${ssrRenderAttr("src", _imports_3)} alt=""></div></div></div></div></div></div></div></section><section id="who" class="block"><div class="container"><div class="section_title"><h2><span>Кому подойдет данный курс</span></h2></div><div class="section_body"><div class="who_body"><div class="row"><div class="col-lg-4 col-sm-6 col-xs-12"><div class="who_block aos-item aos-init aos-animate" data-aos="fade-up"><div class="inner"><div class="thumbnail"><div><img${ssrRenderAttr("src", _imports_4)} alt=""></div></div><div class="content"><span>Начинающим</span><p>Подойдет тем, у кого нет опыта или его мало</p></div></div></div></div><div class="col-lg-4 col-sm-6 col-xs-12"><div class="who_block aos-item aos-init aos-animate" data-aos="fade-up"><div class="inner"><div class="thumbnail"><div><img${ssrRenderAttr("src", _imports_5)} alt=""></div></div><div class="content"><span>Актуальная профессия</span><p>Кто хочет получить востребованную специальность</p></div></div></div></div><div class="col-lg-4 col-sm-6 col-xs-12"><div class="who_block aos-item aos-init aos-animate" data-aos="fade-up"><div class="inner"><div class="thumbnail"><div><img${ssrRenderAttr("src", _imports_6)} alt=""></div></div><div class="content"><span>Повышение навыков</span><p>Для повышения личных навыков и умений, которые идут в портфолио</p></div></div></div></div></div></div></div></div></section><section id="advantages"><div class="elementor-background-overlay"></div><div class="elementor-container"><div class="container"><div class="section_title"><h2><span>Плюсы языка Python</span></h2></div><div class="section_body"><div class="advantages_body"><div class="row"><div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12"><div class="advantages_block aos-item aos-init aos-animate" data-aos="fade-up"><div class="image"><img${ssrRenderAttr("src", _imports_7)} alt=""><div class="icon"><img${ssrRenderAttr("src", _imports_8)} alt=""></div></div><h3>Простота изучения</h3><p>У Python простой синтаксис, который легко читать и понимать. Это делает его идеальным для начинающих программистов.</p></div></div><div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12"><div class="advantages_block aos-item aos-init aos-animate" data-aos="fade-up"><div class="image"><img${ssrRenderAttr("src", _imports_9)} alt=""><div class="icon"><img${ssrRenderAttr("src", _imports_10)} alt=""></div></div><h3>Библиотеки и фреймворки</h3><p>Python имеет много библиотек и фреймворков, которые помогают разработчикам ускорить процесс разработки.</p></div></div><div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12"><div class="advantages_block aos-item aos-init aos-animate" data-aos="fade-up"><div class="image"><img${ssrRenderAttr("src", _imports_11)} alt=""><div class="icon"><img${ssrRenderAttr("src", _imports_12)} alt=""></div></div><h3>Кроссплатформенность</h3><p>Python может работать на разных операционных системах, таких как Windows, Linux, Mac OS и других.</p></div></div><div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12"><div class="advantages_block aos-item aos-init aos-animate" data-aos="fade-up"><div class="image"><img${ssrRenderAttr("src", _imports_13)} alt=""><div class="icon"><img${ssrRenderAttr("src", _imports_14)} alt=""></div></div><h3>Интерактивность</h3><p>Python позволяет быстро тестировать код благодаря интерактивной консоли.</p></div></div></div></div></div></div></div></section><section id="about"><div class="container"><div class="section_body"><div class="row"><div class="col-lg-4 col-xs-12"><div class="about-img" data-aos="fade-up"><img${ssrRenderAttr("src", _imports_15)} alt=""></div></div><div class="col-lg-8 col-xs-12"><div class="about_body" data-aos="fade-up"><div class="section_title"><h2><span>Главное о профессии</span></h2></div><div class="about_text"><p>Python – это универсальный язык программирования, который находит применение в различных областях, начиная от создания веб-сайтов и веб-приложений и заканчивая разработкой алгоритмов машинного обучения и проведением научных исследований.</p><p>Данный курс фокусируется на создании увлекательных игровых сценариев и функциональных приложений, которые захватят воображение и вдохновят на творчество.</p></div></div></div></div></div></div></section><section id="programma"><div class="container"><div class="section_title"><h2><span>Программа обучения</span></h2></div><div class="section_desc"><p>Курс рассчитан на ~36 часов, но заниматься можно в своём темпе. Доступ к материалам останется у вас навсегда — можно в любой момент вернуться, повторить пройденное или проверить себя.</p></div><div class="section_body"><div class="courses"><div class="course" data-aos="fade-up"><ul class="course_m"><li>4 темы</li><li>2 проекта</li><li>6 часов</li></ul><div class="course_desc"><div class="course_title"> Основы Python </div><ul class="course_t"><li>Переменные и типы</li><li>Условия</li><li>Циклы</li><li>Функции</li></ul><div class="icon_course"><img${ssrRenderAttr("src", _imports_16)} alt=""></div></div></div><div class="course" data-aos="fade-up"><ul class="course_m"><li>4 темы</li><li>5 проектов</li><li>12 часов</li></ul><div class="course_desc"><div class="course_title"> Углубленный Python </div><ul class="course_t"><li>Коллекции</li><li>Множества</li><li>Объекты и классы</li><li>Библиотеки</li></ul><div class="icon_course"><img${ssrRenderAttr("src", _imports_17)} alt=""></div></div></div><div class="course" data-aos="fade-up"><ul class="course_m"><li>4 темы</li><li>2 проекта</li><li>18 часов</li></ul><div class="course_desc"><div class="course_title"> Алгоритмы и структуры данных </div><ul class="course_t"><li>Рекурсия</li><li>Сортировка</li><li>Arcades GUI</li><li>PyGame</li></ul><div class="icon_course"><img${ssrRenderAttr("src", _imports_18)} alt=""></div></div></div></div></div></div></section><section id="what"><div class="what-bg"></div><div class="what-body"><div class="container"><div class="section_title"><h2><span>Что Вас ждет?</span></h2></div><div class="what_text"><p>В данном курсе представлен персональный кабинет с обширными материалами уроков. Каждая тема сопровождается тестовыми заданиями и теорией, которые помогут вам углубить свои знания. После завершения каждого модуля предстоит пройти итоговый контроль, чтобы получить доступ к последующим материалам.</p><p>Мы особое внимание уделяем разработке игровых приложений, чтобы сделать ваше обучение увлекательным и интересным. Каждый урок сопровождается созданием игрового приложения, которое поможет вам применить полученные знания на практике.</p><p>По окончании курса вы обретете прочную базу в Python, которая позволит вам более глубоко погрузиться в интересующие вас темы разработки.</p><p>На нашем курсе вы также углубленно изучите создание игр с использованием различных библиотек Python.</p></div></div></div></section><!--]-->`);
}
const _sfc_setup$g = _sfc_main$g.setup;
_sfc_main$g.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Home.vue");
  return _sfc_setup$g ? _sfc_setup$g(props, ctx) : void 0;
};
const Home = /* @__PURE__ */ _export_sfc(_sfc_main$g, [["ssrRender", _sfc_ssrRender$a]]);
const __vite_glob_0_6 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Home
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$f = {};
function _sfc_ssrRender$9(_ctx, _push, _parent, _attrs) {
  _push(`<!--[--><a class="struktura" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample"><span><span></span><span></span><span></span></span></a><div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel"><div class="offcanvas-header"><h5 class="offcanvas-title" id="offcanvasExampleLabel">Содержание</h5><button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Закрыть"></button></div><div class="offcanvas-body"><div><ul class="menu-blocks-list"><li class="menu-block"><h2 class="menu-block__title">1. Введение</h2><ul class="menu-block__items-list"><li class="menu-item menu-item_article"><span class="menu-item__label">1.1.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Как устроен хендбук</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">1.2.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Введение</a></li></ul></li><li class="menu-block"><h2 class="menu-block__title">2. Базовые конструкции Python</h2><ul class="menu-block__items-list"><li class="menu-item menu-item_article"><span class="menu-item__label">2.1.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Ввод и вывод данных. Операции с числами, строками. Форматирование</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">2.2.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Условный оператор</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">2.3.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Циклы</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">2.4.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Вложенные циклы</a></li></ul></li><li class="menu-block"><h2 class="menu-block__title">3. Коллекции и работа с памятью</h2><ul class="menu-block__items-list"><li class="menu-item menu-item_article"><span class="menu-item__label">3.1.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Строки, кортежи, списки</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">3.2.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Множества, словари</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">3.3.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Списочные выражения. Модель памяти для типов языка Python</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">3.4.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Встроенные возможности по работе с коллекциями</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">3.5.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Потоковый ввод/вывод. Работа с текстовыми файлами. JSON</a></li></ul></li><li class="menu-block"><h2 class="menu-block__title">4. Функции и их особенности в Python</h2><ul class="menu-block__items-list"><li class="menu-item menu-item_article"><span class="menu-item__label">4.1.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Функции. Области видимости. Передача параметров в функции</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">4.2.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Позиционные и именованные аргументы. Функции высших порядков. Лямбда-функции</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">4.3.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Рекурсия. Декораторы. Генераторы</a></li></ul></li><li class="menu-block"><h2 class="menu-block__title">5. Объектно-ориентированное программирование</h2><ul class="menu-block__items-list"><li class="menu-item menu-item_article"><span class="menu-item__label">5.1.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Объектная модель Python. Классы, поля и методы</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">5.2.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Волшебные методы, переопределение методов. Наследование</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">5.3.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Модель исключений Python. Try, except, else, finally. Модули</a></li></ul></li><li class="menu-block"><h2 class="menu-block__title">6. Библиотеки для получения и обработки данных</h2><ul class="menu-block__items-list"><li class="menu-item menu-item_article"><span class="menu-item__label">6.1.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Модули math и numpy</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">6.2.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Модуль pandas</a></li><li class="menu-item menu-item_article"><span class="menu-item__label">6.3.</span><a class="menu-item__link" href="/kurs-python-razrabotchik/vvod-i-vyvod-dannykh-operatsii-s-chislami-strokami-formatirovaniye.php">Модуль requests</a></li></ul></li></ul></div></div></div><!--]-->`);
}
const _sfc_setup$f = _sfc_main$f.setup;
_sfc_main$f.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/CourseContent.vue");
  return _sfc_setup$f ? _sfc_setup$f(props, ctx) : void 0;
};
const CourseContent = /* @__PURE__ */ _export_sfc(_sfc_main$f, [["ssrRender", _sfc_ssrRender$9]]);
const _sfc_main$e = {
  props: {
    modelValue: Boolean,
    title: String,
    submitText: String
  },
  components: {
    BModal
  },
  setup(props) {
    const modal = ref(props.modelValue);
    return {
      modal,
      close() {
        modal.value = false;
      }
    };
  }
};
function _sfc_ssrRender$8(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_BModal = resolveComponent("BModal");
  _push(ssrRenderComponent(_component_BModal, mergeProps({
    modelValue: $setup.modal,
    "onUpdate:modelValue": ($event) => $setup.modal = $event,
    title: $props.title,
    centered: ""
  }, _attrs), {
    footer: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        ssrRenderSlot(_ctx.$slots, "actions", {}, null, _push2, _parent2, _scopeId);
      } else {
        return [
          renderSlot(_ctx.$slots, "actions")
        ];
      }
    }),
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        ssrRenderSlot(_ctx.$slots, "content", {}, null, _push2, _parent2, _scopeId);
      } else {
        return [
          renderSlot(_ctx.$slots, "content")
        ];
      }
    }),
    _: 3
  }, _parent));
}
const _sfc_setup$e = _sfc_main$e.setup;
_sfc_main$e.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/DialogModal.vue");
  return _sfc_setup$e ? _sfc_setup$e(props, ctx) : void 0;
};
const DialogModal = /* @__PURE__ */ _export_sfc(_sfc_main$e, [["ssrRender", _sfc_ssrRender$8]]);
const _sfc_main$d = defineComponent({
  components: { DialogModal, Link },
  props: {
    modelValue: Boolean
  },
  setup() {
    return {};
  }
});
function _sfc_ssrRender$7(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_DialogModal = resolveComponent("DialogModal");
  const _component_Link = resolveComponent("Link");
  _push(ssrRenderComponent(_component_DialogModal, mergeProps({
    modelValue: _ctx.modelValue,
    "onUpdate:modelValue": ($event) => _ctx.modelValue = $event
  }, _attrs), {
    content: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` ok `);
      } else {
        return [
          createTextVNode(" ok ")
        ];
      }
    }),
    actions: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_Link, {
          href: "/register",
          class: "btn btn-primary"
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(`Отправить`);
            } else {
              return [
                createTextVNode("Отправить")
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_Link, {
            href: "/register",
            class: "btn btn-primary"
          }, {
            default: withCtx(() => [
              createTextVNode("Отправить")
            ]),
            _: 1
          })
        ];
      }
    }),
    _: 1
  }, _parent));
}
const _sfc_setup$d = _sfc_main$d.setup;
_sfc_main$d.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Modals/Register.vue");
  return _sfc_setup$d ? _sfc_setup$d(props, ctx) : void 0;
};
const Register = /* @__PURE__ */ _export_sfc(_sfc_main$d, [["ssrRender", _sfc_ssrRender$7]]);
const __vite_glob_0_9 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Register
}, Symbol.toStringTag, { value: "Module" }));
const _imports_1 = "/build/assets/f1-C6UirnUl.png";
const _imports_2 = "/build/assets/main-logo-foot-qXgPCzSf.png";
const _sfc_main$c = {
  components: { Register, IonIcon, DialogModal, Link },
  name: "Layout",
  props: {
    session: Object,
    auth: {
      type: Object,
      required: true
    }
  },
  setup(props) {
    const authModalIsOpen = ref(false);
    const authModalKey = ref((/* @__PURE__ */ new Date()).getTime().toString());
    const authModalMessage = ref("");
    const authModalDescription = ref("");
    watch(
      () => props.session,
      (session) => {
        var _a, _b;
        authModalIsOpen.value = !!(session == null ? void 0 : session.requireAuth);
        authModalKey.value = (/* @__PURE__ */ new Date()).getTime().toString();
        authModalMessage.value = (_a = session == null ? void 0 : session.requireAuth) == null ? void 0 : _a.title;
        authModalDescription.value = (_b = session == null ? void 0 : session.requireAuth) == null ? void 0 : _b.description;
      }
    );
    addIcons({
      "arrow-forward-outline": arrowForwardOutline
    });
    window.counter = function() {
      const span = this.querySelector("span");
      const current = parseInt(span.textContent);
      span.textContent = current + 1;
    };
    return {
      authModalIsOpen,
      authModalKey,
      authModalMessage,
      authModalDescription
    };
  }
};
function _sfc_ssrRender$6(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Link = resolveComponent("Link");
  const _component_DialogModal = resolveComponent("DialogModal");
  _push(`<!--[--><div id="wrapper"><section id="welcome"><div id="logo-with-menu"><div class="container"><div class="topline"><div class="logo_top">`);
  _push(ssrRenderComponent(_component_Link, {
    href: "/",
    "preserve-state": ""
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<img${ssrRenderAttr("src", _imports_0$2)} alt=""${_scopeId}>`);
      } else {
        return [
          createVNode("img", {
            src: _imports_0$2,
            alt: ""
          })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<a class="menu-triger" href="#"><span><span></span><span></span><span></span></span></a><div class="menu_top"><ul><li><a href="#advantages" class="scroll-to">О профессиии</a></li><li><a href="#what" class="scroll-to">Что Вас ждет</a></li><li><a href="#programma" class="scroll-to">Программа</a></li></ul></div></div><div class="lk_top d-none d-lg-block"><ul><li>`);
  _push(ssrRenderComponent(_component_Link, {
    href: "/lessons",
    class: "link",
    "preserve-state": ""
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`Моё обучение`);
      } else {
        return [
          createTextVNode("Моё обучение")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</li><li>`);
  _push(ssrRenderComponent(_component_Link, {
    href: "/profile",
    class: "lk",
    "preserve-state": ""
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<img${ssrRenderAttr("src", _imports_1)} alt=""${_scopeId}>`);
      } else {
        return [
          createVNode("img", {
            src: _imports_1,
            alt: ""
          })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</li>`);
  if ($props.auth.user) {
    _push(`<!--[--><li>${ssrInterpolate($props.auth.user.name)}</li><li>`);
    _push(ssrRenderComponent(_component_Link, {
      class: "link",
      href: "/logout",
      method: "post",
      as: "button",
      "preserve-state": ""
    }, {
      default: withCtx((_, _push2, _parent2, _scopeId) => {
        if (_push2) {
          _push2(` Выйти `);
        } else {
          return [
            createTextVNode(" Выйти ")
          ];
        }
      }),
      _: 1
    }, _parent));
    _push(`</li><!--]-->`);
  } else {
    _push(`<!---->`);
  }
  _push(`</ul></div></div></div></div></section>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</div><div id="footer"><div class="container"><div class="foot"><div class="foot_logo"><img${ssrRenderAttr("src", _imports_2)} alt=""></div><div class="footer_menu"><ul><li><a href="#advantages" class="scroll-to">О профессиии</a></li><li><a href="#what" class="scroll-to">Что Вас ждет</a></li><li><a href="#programma" class="scroll-to">Программа</a></li></ul></div></div></div></div>`);
  if ($setup.authModalIsOpen) {
    _push(ssrRenderComponent(_component_DialogModal, {
      modelValue: $setup.authModalIsOpen,
      "onUpdate:modelValue": ($event) => $setup.authModalIsOpen = $event,
      key: $setup.authModalKey,
      title: $setup.authModalMessage
    }, {
      content: withCtx((_, _push2, _parent2, _scopeId) => {
        if (_push2) {
          _push2(`${ssrInterpolate($setup.authModalDescription)}`);
        } else {
          return [
            createTextVNode(toDisplayString($setup.authModalDescription), 1)
          ];
        }
      }),
      actions: withCtx((_, _push2, _parent2, _scopeId) => {
        if (_push2) {
          _push2(ssrRenderComponent(_component_Link, {
            href: "/register",
            class: "btn btn-primary"
          }, {
            default: withCtx((_2, _push3, _parent3, _scopeId2) => {
              if (_push3) {
                _push3(`Регистрация`);
              } else {
                return [
                  createTextVNode("Регистрация")
                ];
              }
            }),
            _: 1
          }, _parent2, _scopeId));
          _push2(ssrRenderComponent(_component_Link, {
            href: "/login",
            class: "btn btn-primary"
          }, {
            default: withCtx((_2, _push3, _parent3, _scopeId2) => {
              if (_push3) {
                _push3(`Вход`);
              } else {
                return [
                  createTextVNode("Вход")
                ];
              }
            }),
            _: 1
          }, _parent2, _scopeId));
        } else {
          return [
            createVNode(_component_Link, {
              href: "/register",
              class: "btn btn-primary"
            }, {
              default: withCtx(() => [
                createTextVNode("Регистрация")
              ]),
              _: 1
            }),
            createVNode(_component_Link, {
              href: "/login",
              class: "btn btn-primary"
            }, {
              default: withCtx(() => [
                createTextVNode("Вход")
              ]),
              _: 1
            })
          ];
        }
      }),
      _: 1
    }, _parent));
  } else {
    _push(`<!---->`);
  }
  _push(`<!--]-->`);
}
const _sfc_setup$c = _sfc_main$c.setup;
_sfc_main$c.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Layouts/Layout.vue");
  return _sfc_setup$c ? _sfc_setup$c(props, ctx) : void 0;
};
const Layout = /* @__PURE__ */ _export_sfc(_sfc_main$c, [["ssrRender", _sfc_ssrRender$6]]);
const _imports_0 = "/build/assets/shape-15-dO-srsOv.png";
const _sfc_main$b = {
  components: { Head, Link, CourseContent },
  layout: Layout
};
function _sfc_ssrRender$5(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Link = resolveComponent("Link");
  const _component_CourseContent = resolveComponent("CourseContent");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Курс «Python-разработчик»" }, null, _parent));
  _push(`<div id="wrapper"><section id="slide-code"><div class="main-banner-area parallax"><div class="container"><div class="main-banner-content block"><h1 data-aos="fade-left"><span>Курс «Python-разработчик»</span></h1><div class="desc_course" data-aos="fade-up"><p>Курс поможет овладеть основным синтаксисом и принципами языка. Для этого не потребуется специальной подготовки — достаточно знаний по информатике, логике и математике на уровне школьной программы. Кроме основных конструкций в курсе рассмотрены разные подходы к программированию, реализованные на Python. А в последней главе вы прикоснётесь к главной суперсиле языка — большому количеству прикладных библиотек для реализации игр.</p></div></div></div></div></section><section id="course"><div class="container"><div id="progress"><div><p>Общий прогресс изучения</p><div class="shkala"><div class="prog_dan"><span class="skolko">0</span><span class="iz">/</span><span class="vsego">20</span><span class="tick">параграфов прочитано</span></div><div class="shape3"><img${ssrRenderAttr("src", _imports_0)} alt=""></div><div class="prog_dan"><span class="skolko">0</span><span class="iz">/</span><span class="vsego">260</span><span class="tick">задач выполнено</span></div></div></div></div><div class="course-body"><ul class="book__info"><li class="book__chapter"><h2 class="book__chapter-title">1. Введение</h2><ul class="book__chapter-articles"><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">1.1.</span><div class="book__article-title">`);
  _push(ssrRenderComponent(_component_Link, { href: "/lessons/1" }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`Как устроен хендбук`);
      } else {
        return [
          createTextVNode("Как устроен хендбук")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">1.2.</span><div class="book__article-title">`);
  _push(ssrRenderComponent(_component_Link, { href: "/lessons/1" }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`Введение`);
      } else {
        return [
          createTextVNode("Введение")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div><div class="book__article-widgets"><div class="article-status ok">Прочитан</div></div></li></ul></li><li class="book__chapter"><h2 class="book__chapter-title">2. Базовые конструкции Python</h2><ul class="book__chapter-articles"><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">2.1.</span><div class="book__article-title"><a href="/lesson">Ввод и вывод данных. Операции с числами, строками. Форматирование</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">2.2.</span><div class="book__article-title"><a href="/lesson">Условный оператор</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">2.3.</span><div class="book__article-title"><a href="/lesson">Циклы</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">2.4.</span><div class="book__article-title"><a href="/lesson">Вложенные циклы</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li></ul></li><li class="book__chapter"><h2 class="book__chapter-title">3. Коллекции и работа с памятью</h2><ul class="book__chapter-articles"><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">3.1.</span><div class="book__article-title"><a href="/lesson">Строки, кортежи, списки</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">3.2.</span><div class="book__article-title"><a href="/lesson">Множества, словари</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">3.3.</span><div class="book__article-title"><a href="/lesson">Списочные выражения. Модель памяти для типов языка Python</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">3.4.</span><div class="book__article-title"><a href="/lesson">Встроенные возможности по работе с коллекциями</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">3.5.</span><div class="book__article-title"><a href="/lesson">Потоковый ввод/вывод. Работа с текстовыми файлами. JSON</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li></ul></li><li class="book__chapter"><h2 class="book__chapter-title">4. Функции и их особенности в Python</h2><ul class="book__chapter-articles"><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">4.1.</span><div class="book__article-title"><a href="/lesson">Функции. Области видимости. Передача параметров в функции</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">4.2.</span><div class="book__article-title"><a href="/lesson">Позиционные и именованные аргументы. Функции высших порядков. Лямбда-функции</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">4.3.</span><div class="book__article-title"><a href="/lesson">Рекурсия. Декораторы. Генераторы</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li></ul></li><li class="book__chapter"><h2 class="book__chapter-title">5. Объектно-ориентированное программирование</h2><ul class="book__chapter-articles"><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">5.1.</span><div class="book__article-title"><a href="/lesson">Объектная модель Python. Классы, поля и методы</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">5.2.</span><div class="book__article-title"><a href="/lesson">Волшебные методы, переопределение методов. Наследование</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">5.3.</span><div class="book__article-title"><a href="/lesson">Модель исключений Python. Try, except, else, finally. Модули</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li></ul></li><li class="book__chapter"><h2 class="book__chapter-title">6. Библиотеки для получения и обработки данных</h2><ul class="book__chapter-articles"><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">6.1.</span><div class="book__article-title"><a href="/lesson">Модули math и numpy</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">6.2.</span><div class="book__article-title"><a href="/lesson">Модуль pandas</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li><li class="book__chapter-article"><div class="book__article-title-block"><span class="book__article-number">6.3.</span><div class="book__article-title"><a href="/lesson">Модуль requests</a></div></div><div class="book__article-widgets"><div class="article-status">Не прочитан</div><div class="article-progress"><span class="article-progress__bar"></span><span><span class="progress__first-number_contest">0</span> / <span class="progress__second-number_contest">20</span><span class="article-progress__label">выполнено</span></span></div></div></li></ul></li></ul></div></div></section></div>`);
  _push(ssrRenderComponent(_component_CourseContent, null, null, _parent));
  _push(`<!--]-->`);
}
const _sfc_setup$b = _sfc_main$b.setup;
_sfc_main$b.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Lessons/Index.vue");
  return _sfc_setup$b ? _sfc_setup$b(props, ctx) : void 0;
};
const Index = /* @__PURE__ */ _export_sfc(_sfc_main$b, [["ssrRender", _sfc_ssrRender$5]]);
const __vite_glob_0_7 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Index
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$a = {
  components: { PrimaryButton, Head, Link, CourseContent },
  layout: Layout,
  props: {
    lesson: {
      type: Object
    },
    isRead: {
      type: Boolean
    }
  },
  setup(props) {
    const linkHref = computed(() => `/lessons/${props.lesson.id}`);
    const lessonTitle = computed(() => `${props.lesson.serial_number} ${props.lesson.name}`);
    const completeBtnText = computed(() => props.isRead ? "Вы уже прочитали этот курс!" : "Я прочитал урок");
    return {
      linkHref,
      lessonTitle,
      lessonCompleteBtnText: completeBtnText
    };
  }
};
function _sfc_ssrRender$4(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Link = resolveComponent("Link");
  const _component_CourseContent = resolveComponent("CourseContent");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: $setup.lessonTitle }, null, _parent));
  _push(`<section id="slide-code"><div class="main-banner-area parallax"><div class="container"><div class="main-banner-content block"><h1 data-aos="fade-left"><span>${ssrInterpolate($setup.lessonTitle)}</span></h1></div></div></div></section><section id="article"><div class="container"><div class="article__content"><p>В данном учебном курсе мы будем писать программы без графического пользовательского интерфейса, то есть без кнопок, текстовых полей ввода и т. д. Программы будут принимать на вход данные от пользователя с клавиатуры (а чуть позже мы научимся работать с файловым вводом и выводом), а выводить результат будут на экран. Всё взаимодействие с программой будет происходить в командной строке, также называемой консолью или терминалом.</p><p>Во введении к курсу мы узнали, что командная строка в среде разработки Visual Studio Code находится внизу на вкладке «Терминал». Именно в ней мы будем вводить данные с клавиатуры, а программа будет выводить результаты. Как было сказано ранее, мы будем писать компьютерные программы, которые принимают на вход данные, работают с ними и выводят результат на экран.</p><p>Вспомним программу из введения:</p><pre class="hljs">                    <code>
                        <span class="hljs-built_in">print</span>
                        (<span class="hljs-string">&quot;Привет, мир!&quot;</span>)
                    </code>
                </pre><p>В этой программе мы вызываем встроенную функцию под названием <code>print()</code>. Как она устроена внутри, нам пока не столь важно. Главное — запомнить, что у неё есть имя, по которому к ней можно обратиться, плюс она принимает данные и обрабатывает их. О том, какими могут быть данные, мы тоже поговорим позже — в нашем примере мы передаём строку «Привет, мир!».</p><p>В Python строки заключены в кавычки — можно использовать одинарные, двойные и тройные (рекомендуется их составлять из трёх двойных). Внутри кавычек одного вида можно использовать другой вид кавычек как часть выводимой на экран строки.</p><p>Например:</p><ul><li><code>print(&#39;Привет, мир!&#39;)</code></li><li><code>print(&quot;Привет, мир!&quot;)</code></li><li><code>print(&quot;&quot;&quot;Привет, мир!&quot;&quot;&quot;)</code></li><li><code>print(&quot;Программа выводит фразу &#39;Привет, мир!&#39;&quot;)</code></li></ul><p>Данные в программу могут поступать различными способами. Самый простой и привычный для пользователя — это ввод с клавиатуры. Для этого в Python используется функция <code>input()</code>, которая возвращает в программу введённую пользователем строку. Но данные нужно куда-то сохранить, и в этом нам помогают переменные. Переменным назначаются имена, в них записываются значения, и затем они используются в программе по мере необходимости.</p><p>Напишем программу, которая получает от пользователя строку и выводит на экран:</p><pre class="hljs">                    <code>phrase = <span class="hljs-built_in">input</span>()
                        <span class="hljs-built_in">print</span>(phrase)
                    </code>
                </pre><p>После запуска программы происходит приостановка выполнения и в терминале появляется приглашение для ввода данных:</p><p><img src="https://yastatic.net/s3/ml-handbook/admin/1_1_ccbffce74e.png" alt="1_1.png"></p><p>После ввода данных необходимо нажать клавишу <code>Enter</code>. Программа запишет введённые данные в переменную <code>phrase</code> и продолжит выполнение со следующей строчки. Переменная начинает существовать в программе, когда в неё записывается какое-то значение. В момент записи значения переменной назначается тип данных, определённый в соответствии со значением.</p><p>В нашем примере в переменную <code>phrase</code> записывается строка, введённая пользователем. Поэтому переменной <code>phrase</code> назначается встроенный в язык Python тип данных <code>str</code>. Тип данных <code>str</code> используется для хранения строк, а сами строки являются упорядоченной последовательностью символов.</p><p>У функции <code>input()</code> можно задать параметр-строку перед вводом значения — и тогда указанный текст будет сопровождать приглашение к вводу данных:</p><pre class="hljs">                    <code>phrase = <span class="hljs-built_in">input</span>(<span class="hljs-string">&quot;Введите строку: &quot;</span>)
                        <span class="hljs-built_in">print</span>(phrase)
                    </code>
                </pre><p><img src="https://yastatic.net/s3/ml-handbook/admin/1_2_91d62f8f9d.png" alt="1_2.png"></p><p>Для именования переменных и правильного оформления кода существует стандарт <a href="https://peps.python.org/pep-0008/">PEP 8</a> (Python Enhancement Proposals), который следует соблюдать. Согласно стандарту PEP 8, имена переменных должны содержать маленькие буквы английского алфавита и символ «подчёркивание» для разделения слов в имени. Пример имён переменных по стандарту: value, first_value.</p><p>Нельзя использовать следующие однобуквенные имена переменных:</p><ul><li>I (большая английская i);</li><li>l (маленькая английская L);</li><li>O.</li></ul><p>Эти однобуквенные имена усложнят читаемость кода, так как могут быть перепутаны между собой или с цифрами. Все стандарты PEP можно посмотреть на <a href="https://peps.python.org/">этой странице</a>.</p><p>Но вернёмся к функции <code>print()</code>. С её помощью можно выводить сразу несколько значений. Для этого их нужно перечислить через запятую. Выведем, например, фразу <code>Добрый день, %имя%.</code>:</p><pre class="hljs">                    <code>name = <span class="hljs-string">&quot;Пользователь&quot;</span>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">&quot;Добрый день,&quot;</span>, name, <span class="hljs-string">&quot;.&quot;</span>)
                    </code>
                </pre><p>В консоли отобразится:</p><pre>Добрый день, Пользователь .</pre><p>Как видим, результат работы программы не соответствует нашим ожиданиям, так как между словом «Пользователь» и точкой появился пробел. Данный пробел является символом, который по умолчанию ставится между выводимыми значениями. Вместо пробела можно указать любую другую строку, даже пустую. Для этого нужно передать в функцию <code>print()</code> <em>именованный аргумент</em> <code>sep</code> (англ. separator — «разделитель»). Обратите внимание: для именованных аргументов вокруг символа &quot;=&quot; не ставятся пробелы. Сделаем так, чтобы вместо пробела в качестве разделителя использовалась пустая строка:</p><pre class="hljs">                    <code>name = <span class="hljs-string">&quot;Пользователь&quot;</span>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">&quot;Добрый день, &quot;</span>, name, <span class="hljs-string">&quot;.&quot;</span>, sep=<span class="hljs-string">&quot;&quot;</span>)
                    </code>
                </pre><p>В консоли отобразится:</p><pre>Добрый день, Пользователь.</pre><p>Но в таком случае необходимые пробелы нам придётся ставить самим, а это неудобно. К счастью, в Python существует удобный и мощный инструмент для форматирования строк — <em>f-строки</em>. Чтобы задать f-строку, необходимо поставить букву f перед открывающей кавычкой строки. Далее f-строка записывается как единое целое, с учётом правил её форматирования, и закрывается соответствующей кавычкой:</p><pre class="hljs">                    <code>name = <span class="hljs-string">&quot;Пользователь&quot;</span>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">f&quot;Добрый день, <span class="hljs-subst">{name}</span>.&quot;</span>)
                    </code>
                </pre><p>f-строки также можно использовать для выравнивания строк. Например, если требуется добавить символы &quot;0&quot; слева (для выравнивания по правому краю), справа (для выравнивания по левому краю) или слева и справа (для выравнивания посередине) от исходной строки до достижения длины в 9 символов:</p><pre class="hljs">                    <code>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">f&quot;<span class="hljs-subst">{<span class="hljs-number">123</span>:<span class="hljs-number">0</span>&gt;<span class="hljs-number">9</span>}</span>&quot;</span>)
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">f&quot;<span class="hljs-subst">{<span class="hljs-number">123</span>:<span class="hljs-number">0</span>&lt;<span class="hljs-number">9</span>}</span>&quot;</span>)
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">f&quot;<span class="hljs-subst">{<span class="hljs-number">123</span>:<span class="hljs-number">0</span>^<span class="hljs-number">9</span>}</span>&quot;</span>)
                    </code>
                </pre><p>В консоли будут напечатаны следующие строки:</p><pre>                    000000123
                    123000000
                    000123000
                </pre><p>Внутри f-строк можно обращаться к переменным, используя фигурные скобки, как в примере выше, а также производить операции, выполнять функции и подставлять их результаты в данную строку. И это только малая часть возможностей f-строк. Более подробно про них можно почитать в данном <a href="https://docs.python.org/3/tutorial/inputoutput.html#formatted-string-literals">источнике</a>.</p><p>Использование f-строк является приоритетным способом форматирования. Наряду с f-строками существует функция <code>format()</code>, которая также предназначена для удобного форматирования (мы рассмотрим её чуть позже). Также форматирование строк может производиться с помощью символа <code>%</code>. Однако данный способ форматирования является устаревшим (<a href="https://docs.python.org/3/tutorial/inputoutput.html#old-string-formatting">здесь</a> можно почитать про него подробнее).</p><p>В строках можно применять управляющие символы, начинающиеся с символа «бэкслеш» <code>\\</code>. Например:</p><ul><li><code>\\n</code> — переход на новую строку; </li><li><code>\\t</code> — табуляция; </li><li><code>\\r</code> — возврат каретки в начало строки; </li><li><code>\\b</code> — возврат каретки на один символ. </li></ul><p>Кроме того, с помощью бэкслеша можно экранировать символы, то есть делать их частью выводимой строки. Например, для вывода символа <code>\\</code> необходимо его экранировать самим собой:</p><pre class="hljs">                    <code>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">&quot;\\\\&quot;</span>)
                    </code>
                </pre><p>Подробнее об экранировании можно почитать в <a href="https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals">этом источнике</a>.</p><p>В функции <code>print()</code> кроме параметра <code>sep</code> существует параметр <code>end</code>, который определяет символ в конце строки. Значение по умолчанию для него — переход на новую строку, обозначаемый как <code>\\n</code>. Если вывести строки с помощью нескольких использований функции <code>print()</code>, то вывод каждой из них будет осуществлён с новой строки:</p><pre class="hljs">                    <code>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">&quot;Привет, Пользователь!&quot;</span>)
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">&quot;Как дела?&quot;</span>)
                    </code>
                </pre><pre>Привет, Пользователь! Как дела?</pre><p>Над строками можно производить следующие операции:</p><ul><li>сложение (конкатенация строк);</li><li>умножение строки на целое число.</li></ul><p>Результатом сложения строк будет новая строка, представляющая собой записанные последовательно складываемые строки (строки как бы склеиваются друг с другом, образуя новую строку).</p><pre class="hljs">                    <code>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">&quot;Сложно&quot;</span> + <span class="hljs-string">&quot;подчинённый&quot;</span>)
                    </code>
                </pre><pre>Сложноподчинённый</pre><p>При умножении строки на целое число <code>n</code> получается новая строка, состоящая из <code>n</code> дублирований исходной строки. Например, выведем 10 символов <code>-</code> подряд:</p><pre class="hljs">                    <code>
                        <span class="hljs-built_in">print</span>(<span class="hljs-string">&quot;-&quot;</span> * <span class="hljs-number">10</span>)
                    </code>
                </pre><p>В одном из следующих параграфов мы познакомимся с другими возможностями Python по работе со строками. А сейчас перейдём к работе с числами. Для создания целочисленной переменной в программе достаточно назначить ей имя и присвоить целочисленное значение. Например:</p><pre class="hljs">                    <code>
                        n = <span class="hljs-number">10</span>
                    </code>
                </pre><p>По аналогии создаются вещественные числовые переменные, только в качестве разделителя целой и дробной частей используется десятичный разделитель «точка»:</p><pre class="hljs">                    <code>
                        pi = <span class="hljs-number">3.14</span>
                    </code>
                </pre><p>Для преобразования строк в числа и наоборот используются следующие функции:</p><ul><li><code>int()</code> — преобразует строку (или вещественное число) в целое число. Дополнительно можно указать, в какой системе счисления было записано исходное число. По умолчанию используется десятичная система. При конвертации вещественного числа в целое отбрасывается дробная часть;</li><li><code>float()</code> — преобразует строку (или целое число) в вещественное число; </li><li><code>str()</code> — преобразует значения (в общем случае не только числовые) в строки. </li></ul></div><hr>`);
  _push(ssrRenderComponent(_component_Link, {
    href: $setup.linkHref,
    method: "put",
    disabled: $props.isRead,
    "preserve-state": "",
    "preserve-scroll": "",
    as: "button"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`${ssrInterpolate($setup.lessonCompleteBtnText)}`);
      } else {
        return [
          createTextVNode(toDisplayString($setup.lessonCompleteBtnText), 1)
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<div class="check-code">`);
  _push(ssrRenderComponent(_component_Link, { href: "/practice" }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`Потренируемся?`);
      } else {
        return [
          createTextVNode("Потренируемся?")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div></section>`);
  _push(ssrRenderComponent(_component_CourseContent, null, null, _parent));
  _push(`<!--]-->`);
}
const _sfc_setup$a = _sfc_main$a.setup;
_sfc_main$a.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Lessons/Lesson.vue");
  return _sfc_setup$a ? _sfc_setup$a(props, ctx) : void 0;
};
const Lesson = /* @__PURE__ */ _export_sfc(_sfc_main$a, [["ssrRender", _sfc_ssrRender$4]]);
const __vite_glob_0_8 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Lesson
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$9 = {
  __name: "CodeArea",
  __ssrInlineRender: true,
  props: { value: String },
  emits: ["updateCode"],
  setup(__props, { emit: __emit }) {
    const props = __props;
    const emits = __emit;
    const code = computed({
      get: () => props.value,
      set: (newValue) => emits("updateCode", newValue)
    });
    const cmOptions = {
      mode: "text/x-python",
      theme: "dracula",
      lineNumbers: true,
      autoCloseBrackets: true
    };
    return (_ctx, _push, _parent, _attrs) => {
      _push(ssrRenderComponent(unref(Codemirror), mergeProps({
        value: code.value,
        "onUpdate:value": ($event) => code.value = $event,
        options: cmOptions,
        border: "",
        id: "code_area",
        placeholder: "код...",
        height: 500
      }, _attrs), null, _parent));
    };
  }
};
const _sfc_setup$9 = _sfc_main$9.setup;
_sfc_main$9.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/CodeArea.vue");
  return _sfc_setup$9 ? _sfc_setup$9(props, ctx) : void 0;
};
const _sfc_main$8 = {
  components: {
    PrimaryButton,
    CodeArea: _sfc_main$9,
    Head,
    CourseContent
  },
  layout: Layout,
  props: {
    code: {
      type: String,
      required: false,
      default: ""
    },
    output: {
      type: String,
      required: false,
      default: ""
    }
  },
  setup(props) {
    const form = useForm({});
    const code = ref("");
    const submit = () => {
      if (code.value === "") {
        return;
      }
      form.transform((data) => ({
        ...data,
        codeArea: code.value
      })).post("/practice", {
        preserveScroll: true
      });
    };
    function onCodeChanged(value) {
      code.value = value;
    }
    return {
      form,
      code,
      submit,
      onCodeChanged
    };
  }
};
function _sfc_ssrRender$3(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_CodeArea = resolveComponent("CodeArea");
  const _component_PrimaryButton = resolveComponent("PrimaryButton");
  const _component_CourseContent = resolveComponent("CourseContent");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Практическое задание" }, null, _parent));
  _push(`<div id="wrapper"><section id="slide-code"><div class="main-banner-area parallax"><div class="container"><div class="main-banner-content block"><h1 data-aos="fade-left"><span>Проверка кода</span></h1></div></div></div></section><section id="check-code"><div class="container"><div class="block_top"><div class="zadanie"><div class="zad_title"> Описание задачи </div><div class="zad_telo"> Когда мы приходим на встречу, то первым делом здороваемся. Давайте тоже поприветствуемся. </div></div><div class="format"><div class="zad_title"> Формат вывода </div><div class="zad_telo"> Одна строка: «Привет, Яндекс!» </div></div><span class="zad_status">Неверное решение</span></div><div class="block_bottom">`);
  _push(ssrRenderComponent(_component_CodeArea, {
    value: $setup.code,
    onUpdateCode: $setup.onCodeChanged
  }, null, _parent));
  _push(`<textarea style="${ssrRenderStyle({ "max-width": "50%", "height": "300px", "border-color": "#1a1d20" })}" disabled>${ssrInterpolate($props.output)}</textarea><div class="flex justify-start mt-4">`);
  _push(ssrRenderComponent(_component_PrimaryButton, {
    onClick: $setup.submit,
    class: { "opacity-25": $setup.form.processing },
    disabled: $setup.form.processing
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(` Выполнить `);
      } else {
        return [
          createTextVNode(" Выполнить ")
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div></div></section></div>`);
  _push(ssrRenderComponent(_component_CourseContent, null, null, _parent));
  _push(`<!--]-->`);
}
const _sfc_setup$8 = _sfc_main$8.setup;
_sfc_main$8.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Practice/Practice.vue");
  return _sfc_setup$8 ? _sfc_setup$8(props, ctx) : void 0;
};
const Practice = /* @__PURE__ */ _export_sfc(_sfc_main$8, [["ssrRender", _sfc_ssrRender$3]]);
const __vite_glob_0_10 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Practice
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$7 = {
  props: {
    topUsers: {
      type: Array
    },
    currentUserId: {
      type: Number
    }
  },
  setup(props) {
    const isMe = (id) => id == props.currentUserId;
    const prepareUserTop = (index) => {
      const top = toNumber(index) + 1;
      return `ТОП-${top} `;
    };
    const prepareUserLessonsCount = (count) => ` (пройдено тем: ${count})`;
    return {
      isMe,
      prepareUserTop,
      prepareUserLessonsCount
    };
  }
};
function _sfc_ssrRender$2(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<!--[--><section data-v-06820f5a><header data-v-06820f5a><h2 class="text-lg font-medium text-gray-900" data-v-06820f5a>Рейтинговая таблица</h2></header></section><div class="grid-area-2" data-v-06820f5a><section class="friend-acitvity" data-v-06820f5a><div class="friend-activity__activity" data-v-06820f5a><!--[-->`);
  ssrRenderList($props.topUsers, (user, index) => {
    _push(`<h3 class="friend-activity__row" data-v-06820f5a><span class="friend-activity__rating" data-v-06820f5a>${ssrInterpolate($setup.prepareUserTop(index))}</span><span class="friend-activity__name" data-v-06820f5a>${ssrInterpolate(user.name)}</span><span class="friend-activity__msg" data-v-06820f5a>${ssrInterpolate($setup.prepareUserLessonsCount(user.lessons_count))}</span>`);
    if ($setup.isMe(user.id)) {
      _push(`<span class="" data-v-06820f5a> это ты</span>`);
    } else {
      _push(`<!---->`);
    }
    _push(`</h3>`);
  });
  _push(`<!--]--></div></section></div><!--]-->`);
}
const _sfc_setup$7 = _sfc_main$7.setup;
_sfc_main$7.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Profile/Partials/RatingTable.vue");
  return _sfc_setup$7 ? _sfc_setup$7(props, ctx) : void 0;
};
const RatingTable = /* @__PURE__ */ _export_sfc(_sfc_main$7, [["ssrRender", _sfc_ssrRender$2], ["__scopeId", "data-v-06820f5a"]]);
const __vite_glob_0_13 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: RatingTable
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$6 = {};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs) {
  _push(`<button${ssrRenderAttrs(mergeProps({ class: "inline-flex items-center px-4 py-2 bg-red-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-500 active:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-offset-2 transition ease-in-out duration-150" }, _attrs))}>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</button>`);
}
const _sfc_setup$6 = _sfc_main$6.setup;
_sfc_main$6.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/DangerButton.vue");
  return _sfc_setup$6 ? _sfc_setup$6(props, ctx) : void 0;
};
const DangerButton = /* @__PURE__ */ _export_sfc(_sfc_main$6, [["ssrRender", _sfc_ssrRender$1]]);
const _sfc_main$5 = {
  __name: "Modal",
  __ssrInlineRender: true,
  props: {
    show: {
      type: Boolean,
      default: false
    },
    maxWidth: {
      type: String,
      default: "2xl"
    },
    closeable: {
      type: Boolean,
      default: true
    }
  },
  emits: ["close"],
  setup(__props, { emit: __emit }) {
    const props = __props;
    const emit = __emit;
    watch(
      () => props.show,
      () => {
        if (props.show) {
          document.body.style.overflow = "hidden";
        } else {
          document.body.style.overflow = null;
        }
      }
    );
    const close = () => {
      if (props.closeable) {
        emit("close");
      }
    };
    const closeOnEscape = (e) => {
      if (e.key === "Escape" && props.show) {
        close();
      }
    };
    onMounted(() => document.addEventListener("keydown", closeOnEscape));
    onUnmounted(() => {
      document.removeEventListener("keydown", closeOnEscape);
      document.body.style.overflow = null;
    });
    const maxWidthClass = computed(() => {
      return {
        sm: "sm:max-w-sm",
        md: "sm:max-w-md",
        lg: "sm:max-w-lg",
        xl: "sm:max-w-xl",
        "2xl": "sm:max-w-2xl"
      }[props.maxWidth];
    });
    return (_ctx, _push, _parent, _attrs) => {
      ssrRenderTeleport(_push, (_push2) => {
        _push2(`<div style="${ssrRenderStyle(__props.show ? null : { display: "none" })}" class="fixed inset-0 overflow-y-auto px-4 py-6 sm:px-0 z-50" scroll-region><div style="${ssrRenderStyle(__props.show ? null : { display: "none" })}" class="fixed inset-0 transform transition-all"><div class="absolute inset-0 bg-gray-500 opacity-75"></div></div><div style="${ssrRenderStyle(__props.show ? null : { display: "none" })}" class="${ssrRenderClass([maxWidthClass.value, "mb-6 bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:w-full sm:mx-auto"])}">`);
        if (__props.show) {
          ssrRenderSlot(_ctx.$slots, "default", {}, null, _push2, _parent);
        } else {
          _push2(`<!---->`);
        }
        _push2(`</div></div>`);
      }, "body", false, _parent);
    };
  }
};
const _sfc_setup$5 = _sfc_main$5.setup;
_sfc_main$5.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/Modal.vue");
  return _sfc_setup$5 ? _sfc_setup$5(props, ctx) : void 0;
};
const _sfc_main$4 = {
  __name: "SecondaryButton",
  __ssrInlineRender: true,
  props: {
    type: {
      type: String,
      default: "button"
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<button${ssrRenderAttrs(mergeProps({
        type: __props.type,
        class: "inline-flex items-center px-4 py-2 bg-white border border-gray-300 rounded-md font-semibold text-xs text-gray-700 uppercase tracking-widest shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 disabled:opacity-25 transition ease-in-out duration-150"
      }, _attrs))}>`);
      ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
      _push(`</button>`);
    };
  }
};
const _sfc_setup$4 = _sfc_main$4.setup;
_sfc_main$4.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/SecondaryButton.vue");
  return _sfc_setup$4 ? _sfc_setup$4(props, ctx) : void 0;
};
const _sfc_main$3 = {
  __name: "DeleteUserForm",
  __ssrInlineRender: true,
  setup(__props) {
    const confirmingUserDeletion = ref(false);
    const passwordInput = ref(null);
    const form = useForm({
      password: ""
    });
    const confirmUserDeletion = () => {
      confirmingUserDeletion.value = true;
      nextTick(() => passwordInput.value.focus());
    };
    const deleteUser = () => {
      form.delete("/profile", {
        preserveScroll: true,
        onSuccess: () => closeModal(),
        onError: () => passwordInput.value.focus(),
        onFinish: () => form.reset()
      });
    };
    const closeModal = () => {
      confirmingUserDeletion.value = false;
      form.reset();
    };
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<section${ssrRenderAttrs(mergeProps({ class: "space-y-6" }, _attrs))}><header><h2 class="text-lg font-medium text-gray-900">Удаление учетной записи</h2><p class="mt-1 text-sm text-gray-600"> При удалении учетной записи все Ваши данные будут потеряны. </p></header>`);
      _push(ssrRenderComponent(DangerButton, { onClick: confirmUserDeletion }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`Удалить учетную запись`);
          } else {
            return [
              createTextVNode("Удалить учетную запись")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(ssrRenderComponent(_sfc_main$5, {
        show: confirmingUserDeletion.value,
        onClose: closeModal
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`<div class="p-6"${_scopeId}><h2 class="text-lg font-medium text-gray-900"${_scopeId}> Вы уверены, что хотите удалить учетную запись? </h2><p class="mt-1 text-sm text-gray-600"${_scopeId}> При удалении учетной записи все Ваши данные будут потеряны. </p><div class="mt-6"${_scopeId}>`);
            _push2(ssrRenderComponent(_sfc_main$q, {
              for: "password",
              value: "Пароль",
              class: "sr-only"
            }, null, _parent2, _scopeId));
            _push2(ssrRenderComponent(_sfc_main$o, {
              id: "password",
              ref_key: "passwordInput",
              ref: passwordInput,
              modelValue: unref(form).password,
              "onUpdate:modelValue": ($event) => unref(form).password = $event,
              type: "password",
              class: "mt-1 block w-3/4",
              placeholder: "*******",
              onKeyup: deleteUser
            }, null, _parent2, _scopeId));
            _push2(ssrRenderComponent(_sfc_main$r, {
              message: unref(form).errors.password,
              class: "mt-2"
            }, null, _parent2, _scopeId));
            _push2(`</div><div class="mt-6 flex justify-end"${_scopeId}>`);
            _push2(ssrRenderComponent(_sfc_main$4, { onClick: closeModal }, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(` Закрыть `);
                } else {
                  return [
                    createTextVNode(" Закрыть ")
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(DangerButton, {
              class: ["ms-3", { "opacity-25": unref(form).processing }],
              disabled: unref(form).processing,
              onClick: deleteUser
            }, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(` Удалить аккаунт `);
                } else {
                  return [
                    createTextVNode(" Удалить аккаунт ")
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(`</div></div>`);
          } else {
            return [
              createVNode("div", { class: "p-6" }, [
                createVNode("h2", { class: "text-lg font-medium text-gray-900" }, " Вы уверены, что хотите удалить учетную запись? "),
                createVNode("p", { class: "mt-1 text-sm text-gray-600" }, " При удалении учетной записи все Ваши данные будут потеряны. "),
                createVNode("div", { class: "mt-6" }, [
                  createVNode(_sfc_main$q, {
                    for: "password",
                    value: "Пароль",
                    class: "sr-only"
                  }),
                  createVNode(_sfc_main$o, {
                    id: "password",
                    ref_key: "passwordInput",
                    ref: passwordInput,
                    modelValue: unref(form).password,
                    "onUpdate:modelValue": ($event) => unref(form).password = $event,
                    type: "password",
                    class: "mt-1 block w-3/4",
                    placeholder: "*******",
                    onKeyup: withKeys(deleteUser, ["enter"])
                  }, null, 8, ["modelValue", "onUpdate:modelValue"]),
                  createVNode(_sfc_main$r, {
                    message: unref(form).errors.password,
                    class: "mt-2"
                  }, null, 8, ["message"])
                ]),
                createVNode("div", { class: "mt-6 flex justify-end" }, [
                  createVNode(_sfc_main$4, { onClick: closeModal }, {
                    default: withCtx(() => [
                      createTextVNode(" Закрыть ")
                    ]),
                    _: 1
                  }),
                  createVNode(DangerButton, {
                    class: ["ms-3", { "opacity-25": unref(form).processing }],
                    disabled: unref(form).processing,
                    onClick: deleteUser
                  }, {
                    default: withCtx(() => [
                      createTextVNode(" Удалить аккаунт ")
                    ]),
                    _: 1
                  }, 8, ["class", "disabled"])
                ])
              ])
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</section>`);
    };
  }
};
const _sfc_setup$3 = _sfc_main$3.setup;
_sfc_main$3.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Profile/Partials/DeleteUserForm.vue");
  return _sfc_setup$3 ? _sfc_setup$3(props, ctx) : void 0;
};
const __vite_glob_0_12 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: _sfc_main$3
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$2 = {
  __name: "UpdatePasswordForm",
  __ssrInlineRender: true,
  setup(__props) {
    const passwordInput = ref(null);
    const currentPasswordInput = ref(null);
    const form = useForm({
      current_password: "",
      password: "",
      password_confirmation: ""
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<section${ssrRenderAttrs(_attrs)}><header><h2 class="text-lg font-medium text-gray-900">Изменение пароля</h2><p class="mt-1 text-sm text-gray-600"> Для обеспечения безопасности Вашей учетной записи рекомендуем использовать длинный пароль. </p></header><form class="mt-6 space-y-6"><div>`);
      _push(ssrRenderComponent(_sfc_main$q, {
        for: "current_password",
        value: "Текущий пароль"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$o, {
        id: "current_password",
        ref_key: "currentPasswordInput",
        ref: currentPasswordInput,
        modelValue: unref(form).current_password,
        "onUpdate:modelValue": ($event) => unref(form).current_password = $event,
        type: "password",
        class: "mt-1 block w-full",
        placeholder: "**********"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$r, {
        message: unref(form).errors.current_password,
        class: "mt-2"
      }, null, _parent));
      _push(`</div><div>`);
      _push(ssrRenderComponent(_sfc_main$q, {
        for: "password",
        value: "Новый пароль"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$o, {
        id: "password",
        ref_key: "passwordInput",
        ref: passwordInput,
        modelValue: unref(form).password,
        "onUpdate:modelValue": ($event) => unref(form).password = $event,
        type: "password",
        class: "mt-1 block w-full",
        placeholder: "**********"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$r, {
        message: unref(form).errors.password,
        class: "mt-2"
      }, null, _parent));
      _push(`</div><div>`);
      _push(ssrRenderComponent(_sfc_main$q, {
        for: "password_confirmation",
        value: "Подтвердите пароль"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$o, {
        id: "password_confirmation",
        modelValue: unref(form).password_confirmation,
        "onUpdate:modelValue": ($event) => unref(form).password_confirmation = $event,
        type: "password",
        class: "mt-1 block w-full",
        placeholder: "**********"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$r, {
        message: unref(form).errors.password_confirmation,
        class: "mt-2"
      }, null, _parent));
      _push(`</div><div class="flex items-center gap-4">`);
      _push(ssrRenderComponent(PrimaryButton, {
        disabled: unref(form).processing
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`Сохранить`);
          } else {
            return [
              createTextVNode("Сохранить")
            ];
          }
        }),
        _: 1
      }, _parent));
      if (unref(form).recentlySuccessful) {
        _push(`<p class="text-sm text-gray-600">Успешно.</p>`);
      } else {
        _push(`<!---->`);
      }
      _push(`</div></form></section>`);
    };
  }
};
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Profile/Partials/UpdatePasswordForm.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const __vite_glob_0_14 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: _sfc_main$2
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main$1 = {
  __name: "UpdateProfileInformationForm",
  __ssrInlineRender: true,
  props: {
    mustVerifyEmail: {
      type: Boolean
    },
    status: {
      type: String
    }
  },
  setup(__props) {
    const user = usePage().props.auth.user;
    const form = useForm({
      name: user.name,
      email: user.email
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<section${ssrRenderAttrs(_attrs)}><header><h2 class="text-lg font-medium text-gray-900">Профиль</h2><p class="mt-1 text-sm text-gray-600"> Редактирование профиля </p></header><form class="mt-6 space-y-6"><div>`);
      _push(ssrRenderComponent(_sfc_main$q, {
        for: "name",
        value: "ФИО"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$o, {
        id: "name",
        type: "text",
        class: "mt-1 block w-full",
        modelValue: unref(form).name,
        "onUpdate:modelValue": ($event) => unref(form).name = $event,
        required: "",
        autofocus: "",
        placeholder: "Иванов Иван Иванович"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$r, {
        class: "mt-2",
        message: unref(form).errors.name
      }, null, _parent));
      _push(`</div><div>`);
      _push(ssrRenderComponent(_sfc_main$q, {
        for: "email",
        value: "Почта"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$o, {
        id: "email",
        type: "email",
        class: "mt-1 block w-full",
        modelValue: unref(form).email,
        "onUpdate:modelValue": ($event) => unref(form).email = $event,
        required: "",
        placeholder: "example@gmail.com"
      }, null, _parent));
      _push(ssrRenderComponent(_sfc_main$r, {
        class: "mt-2",
        message: unref(form).errors.email
      }, null, _parent));
      _push(`</div>`);
      if (__props.mustVerifyEmail && unref(user).email_verified_at === null) {
        _push(`<div><p class="text-sm mt-2 text-gray-800"> Вы еще не подтвердили почту. `);
        _push(ssrRenderComponent(unref(Link), {
          href: "/email/verification-notification",
          method: "post",
          class: "underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        }, {
          default: withCtx((_, _push2, _parent2, _scopeId) => {
            if (_push2) {
              _push2(` Нажмите, чтобы отправить ссылку для подтверждения электронной почты. `);
            } else {
              return [
                createTextVNode(" Нажмите, чтобы отправить ссылку для подтверждения электронной почты. ")
              ];
            }
          }),
          _: 1
        }, _parent));
        _push(`</p><div style="${ssrRenderStyle(__props.status === "verification-link-sent" ? null : { display: "none" })}" class="mt-2 font-medium text-sm text-green-600"> Новая ссылка для подтверждение была отправлена на Вашу почту. </div></div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`<div class="flex items-center gap-4">`);
      _push(ssrRenderComponent(PrimaryButton, {
        disabled: unref(form).processing
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`Сохранить`);
          } else {
            return [
              createTextVNode("Сохранить")
            ];
          }
        }),
        _: 1
      }, _parent));
      if (unref(form).recentlySuccessful) {
        _push(`<p class="text-sm text-gray-600">Успешно.</p>`);
      } else {
        _push(`<!---->`);
      }
      _push(`</div></form></section>`);
    };
  }
};
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Profile/Partials/UpdateProfileInformationForm.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __vite_glob_0_15 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: _sfc_main$1
}, Symbol.toStringTag, { value: "Module" }));
const _sfc_main = {
  layout: Layout,
  components: { Head, UpdateProfileInformationForm: _sfc_main$1, UpdatePasswordForm: _sfc_main$2, DeleteUserForm: _sfc_main$3, RatingTable },
  props: {
    mustVerifyEmail: {
      type: Boolean
    },
    status: {
      type: String
    },
    topUsers: {
      type: Array
    }
  },
  setup(props) {
    const curUserId = usePage().props.auth.user.id;
    return { curUserId };
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_RatingTable = resolveComponent("RatingTable");
  const _component_UpdateProfileInformationForm = resolveComponent("UpdateProfileInformationForm");
  const _component_UpdatePasswordForm = resolveComponent("UpdatePasswordForm");
  const _component_DeleteUserForm = resolveComponent("DeleteUserForm");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Профиль" }, null, _parent));
  _push(`<div class="py-12"><div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6"><div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">`);
  _push(ssrRenderComponent(_component_RatingTable, {
    "top-users": $props.topUsers,
    "current-user-id": $setup.curUserId
  }, null, _parent));
  _push(`</div><div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">`);
  _push(ssrRenderComponent(_component_UpdateProfileInformationForm, {
    "must-verify-email": $props.mustVerifyEmail,
    status: $props.status,
    class: "max-w-xl"
  }, null, _parent));
  _push(`</div><div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">`);
  _push(ssrRenderComponent(_component_UpdatePasswordForm, { class: "max-w-xl" }, null, _parent));
  _push(`</div><div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">`);
  _push(ssrRenderComponent(_component_DeleteUserForm, { class: "max-w-xl" }, null, _parent));
  _push(`</div></div></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Profile/Edit.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Edit = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
const __vite_glob_0_11 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Edit
}, Symbol.toStringTag, { value: "Module" }));
createServer((page) => createInertiaApp({
  page,
  render: renderToString,
  resolve: (name) => {
    const pages = /* @__PURE__ */ Object.assign({ "./Pages/Auth/ConfirmPassword.vue": __vite_glob_0_0, "./Pages/Auth/ForgotPassword.vue": __vite_glob_0_1, "./Pages/Auth/Login.vue": __vite_glob_0_2, "./Pages/Auth/Register.vue": __vite_glob_0_3, "./Pages/Auth/ResetPassword.vue": __vite_glob_0_4, "./Pages/Auth/VerifyEmail.vue": __vite_glob_0_5, "./Pages/Home.vue": __vite_glob_0_6, "./Pages/Lessons/Index.vue": __vite_glob_0_7, "./Pages/Lessons/Lesson.vue": __vite_glob_0_8, "./Pages/Modals/Register.vue": __vite_glob_0_9, "./Pages/Practice/Practice.vue": __vite_glob_0_10, "./Pages/Profile/Edit.vue": __vite_glob_0_11, "./Pages/Profile/Partials/DeleteUserForm.vue": __vite_glob_0_12, "./Pages/Profile/Partials/RatingTable.vue": __vite_glob_0_13, "./Pages/Profile/Partials/UpdatePasswordForm.vue": __vite_glob_0_14, "./Pages/Profile/Partials/UpdateProfileInformationForm.vue": __vite_glob_0_15 });
    return pages[`./Pages/${name}.vue`];
  },
  setup({ app, props, plugin }) {
    return createSSRApp({
      render: () => h(app, props)
    }).use(plugin);
  }
}));
