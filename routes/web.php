<?php

use App\Http\Controllers\Home\IndexController;
use App\Http\Controllers\Lesson\LessonController;
use App\Http\Controllers\Practice\PracticeController;
use App\Http\Controllers\Profile\ProfileController;
use Illuminate\Support\Facades\Route;

// Home
Route::get('/', [IndexController::class, 'index'])->name('home');

// Profile
Route::controller(ProfileController::class)
    ->middleware('auth')
    ->group(function () {
        Route::get('/profile', 'index')->name('profile.index');
        Route::patch('/profile', 'update')->name('profile.update');
        Route::delete('/profile', 'destroy')->name('profile.destroy');
    });

// Practice
Route::controller(PracticeController::class)
    ->middleware('auth')
    ->group(function () {
        Route::get('/practice', 'index')->name('practice.index');
        Route::post('/practice', 'store')->name('practice.index');
    });

Route::controller(LessonController::class)
    ->middleware('auth')
    ->group(function () {
        Route::get('/lessons', 'index')->name('lesson.index');
        Route::get('/lessons/{id}', 'show')->name('lesson.show');
        Route::put('/lessons/{id}', 'complete')->name('lesson.complete')->whereNumber('id');
    });

require __DIR__.'/auth.php';
