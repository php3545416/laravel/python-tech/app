SERVICE_NAME=$(notdir $(shell pwd))
DEPLOYER_MAKE=$(MAKE) -C ../../deployer

exec:
	@${DEPLOYER_MAKE} exec-service ${SERVICE_NAME}

rebuild:
	@${DEPLOYER_MAKE} rebuild-services ${SERVICE_NAME}

composer-install:
	@${DEPLOYER_MAKE} call-service SERVICE=${SERVICE_NAME} ACTION="composer install"

run:
	@${DEPLOYER_MAKE} run-services ${SERVICE_NAME}

stop:
	@${DEPLOYER_MAKE} stop-services ${SERVICE_NAME}
