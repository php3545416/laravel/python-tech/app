<?php

namespace Database\Seeders;

use App\Models\Lesson;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LessonSeeder extends Seeder
{
    public function run(): void
    {
        $lessonsBySections = collect($this->getLessonsData());

        $lessonsData = [];

        $lessonsBySections->map(function (array $lessons, int $section) use (&$lessonsData) {
            $lessonsData = array_merge(
                array_map(fn ($lesson) => [...$lesson, ...['section_id' => $section]], $lessons),
                $lessonsData,
            );
        });

        Lesson::query()->insert($lessonsData);
    }

    private function getLessonsData(): array
    {
        return [
            1 => [
                [
                    'id' => 1,
                    'serial_number' => '1.1.',
                    'name' => $name = 'Как устроен хендбук',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => false,
                ],
                [
                    'id' => 2,
                    'serial_number' => '1.2.',
                    'name' => $name = 'Введение',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => false,
                ],
            ],
            2 => [
                [
                    'id' => 3,
                    'serial_number' => '2.1.',
                    'name' => $name = 'Ввод и вывод данных. Операции с числами, строками. Форматирование',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 4,
                    'serial_number' => '2.2.',
                    'name' => $name = 'Условный оператор',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 5,
                    'serial_number' => '2.3.',
                    'name' => $name = 'Циклы',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 6,
                    'serial_number' => '2.4.',
                    'name' => $name = 'Вложенные циклы',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
            ],
            3 => [
                [
                    'id' => 7,
                    'serial_number' => '3.1.',
                    'name' => $name = 'Строки, кортежи, списки',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 8,
                    'serial_number' => '3.2.',
                    'name' => $name = 'Множества, словари',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 9,
                    'serial_number' => '3.3.',
                    'name' => $name = 'Списочные выражения. Модель памяти для типов языка Python',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 10,
                    'serial_number' => '3.4.',
                    'name' => $name = 'Встроенные возможности по работе с коллекциями',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 11,
                    'serial_number' => '3.5.',
                    'name' => $name = 'Потоковый ввод/вывод. Работа с текстовыми файлами. JSON',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
            ],
            4 => [
                [
                    'id' => 12,
                    'serial_number' => '4.1.',
                    'name' => $name = 'Функции. Области видимости. Передача параметров в функции',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 13,
                    'serial_number' => '4.2.',
                    'name' => $name = 'Позиционные и именованные аргументы. Функции высших порядков. Лямбда-функции',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 14,
                    'serial_number' => '4.3.',
                    'name' => $name = 'Рекурсия. Декораторы. Генераторы',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
            ],
            5 => [
                [
                    'id' => 15,
                    'serial_number' => '5.1.',
                    'name' => $name = 'Объектная модель Python. Классы, поля и методы',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 16,
                    'serial_number' => '5.2.',
                    'name' => $name = 'Волшебные методы, переопределение методов. Наследование',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 17,
                    'serial_number' => '5.3.',
                    'name' => $name = 'Модель исключений Python. Try, except, else, finally. Модулие',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
            ],
            6 => [
                [
                    'id' => 18,
                    'serial_number' => '6.1.',
                    'name' => $name = 'Модули math и numpy',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 19,
                    'serial_number' => '6.2.',
                    'name' => $name = 'Модуль pandas',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
                [
                    'id' => 20,
                    'serial_number' => '6.3.',
                    'name' => $name = 'Модуль requests',
                    'code' => Str::slug($name),
                    'is_active' => true,
                    'with_practice' => true,
                ],
            ],
        ];
    }
}
