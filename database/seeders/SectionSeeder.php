<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SectionSeeder extends Seeder
{
    public function run(): void
    {
        Section::query()->insert($this->getSectionData());
    }

    private function getSectionData(): array
    {
        return [
            [
                'serial_number' => '1.',
                'name' => $name = 'Введение',
                'code' => Str::slug($name),
                'is_active' => true,
            ],
            [
                'serial_number' => '2.',
                'name' => $name = 'Базовые конструкции Python',
                'code' => Str::slug($name),
                'is_active' => true,
            ],
            [
                'serial_number' => '3.',
                'name' => $name = 'Коллекции и работа с памятью',
                'code' => Str::slug($name),
                'is_active' => true,
            ],
            [
                'serial_number' => '4.',
                'name' => $name = 'Функции и их особенности в Python',
                'code' => Str::slug($name),
                'is_active' => true,
            ],
            [
                'serial_number' => '5.',
                'name' => $name = 'Объектно-ориентированное программирование',
                'code' => Str::slug($name),
                'is_active' => true,
            ],
            [
                'serial_number' => '6.',
                'name' => $name = 'Библиотеки для получения и обработки данных',
                'code' => Str::slug($name),
                'is_active' => true,
            ],
        ];
    }
}
