<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('code')->unique();
            $table->boolean('is_active');
            $table->string('serial_number');
        });

        Schema::create('lessons', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('code')->unique();
            $table->boolean('is_active');
            $table->string('serial_number');
            $table->string('description')->nullable();
            $table->boolean('with_practice')->default(true);

            $table->foreignId('section_id')->constrained('sections')->cascadeOnDelete();
        });

        Schema::create('problems', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->unique();
            $table->text('answer');
            $table->boolean('is_active');
            $table->string('description');

            $table->foreignId('lesson_id')->constrained('lessons')->cascadeOnDelete();

            $table->timestamps();
        });

        Schema::create('lesson_user', function (Blueprint $table) {
            $table->foreignId('lesson_id')->constrained('lessons')->cascadeOnDelete();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();

            $table->primary(['user_id', 'lesson_id']);
        });

        Schema::create('problem_user', function (Blueprint $table) {
            $table->foreignId('problem_id')->constrained('problems')->cascadeOnDelete();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();

            $table->primary(['user_id', 'problem_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('problem_user');
        Schema::dropIfExists('lesson_user');
        Schema::dropIfExists('problems');
        Schema::dropIfExists('lessons');
        Schema::dropIfExists('sections');
    }
};
